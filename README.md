RESEARCH MATERIAL
=================

_**COMPUTATIONAL AND EXPERIMENTAL ANALYSIS OF THE RECRUITMENT OF UPPER-LIMB MOTONEURONS WITH EPIDURAL ELECTRICAL STIMULATION OF THE PRIMATE CERVICAL SPINAL CORD**_

Nathan GREINER{1,2}, Beatrice BARRA{2}, Giuseppe SCHIAVONE{3}, Nicholas JAMES{1}, Florian FALLEGER{3}, Simon BORGOGNON{1,2}, Sara CONTI{2}, Melanie KAESER{1}, Etienne PRALONG{4}, Eric SCHMIDLIN{2}, Stephanie LACOUR{3}, Jocelyne BLOCH{4}, Grégoire COURTINE{1} and Marco CAPOGROSSO{2}

_1. Center for Neuroprosthetics and Brain Mind Institute, School of Life Sciences, École Polytechnique Fédérale de Lausanne, Geneva, Switzerland_  
_2. Department of Neuroscience and Movement Science, Faculty of Science and Medicine, University of Fribourg, Fribourg, Switzerland_  
_3. Bertarelli Foundation Chair in Neuroprosthetic Technology, Laboratory for Soft Bioelectronics Interface, Institute of Microengineering, Institute of Bioengineering, Centre for Neuroprosthetics, Ecole Polytechnique Fédérale de Lausanne, Geneva, Switzerland_  
_4. Neurosurgery Department, Centre Hospitalier Universitaire Vaudois (CHUV), Lausanne, Switzerland_


CONTENT
-------
The present repository contains the computer code and resources necessary to reproduce the simulation results presented in the article.

The experimental data used in the comparison of Figure 6h is also included (see README.txt in the _experimental_data_ folder).

The actual simulation results presented in the article are not included in this repository (they represent several tens of gigabytes of data). They may be delivered upon specific requests.


REQUIREMENTS
------------
To run the computer code located here, you will need to download the two following packages:

- <https://bitbucket.org/ngreiner/fem_smc_ees/src/master/>
- <https://bitbucket.org/ngreiner/biophy_smc_ees/src/master/>

The first package is written in Matlab (The Mathworks, Inc.) and makes use of Comsol (Burlington MA). It is compatible with:

- Matlab R2015b
- Comsol v5.2a
- macOS 10.13.6

The second package is written in Python and makes use of the _neuron_ simulator. It is compatible with:

- Python 3.7.x
- NEURON v7.7
- macOS 10.13.6

On UNIX-based system, these packages can be downloaded by running the following commands in a terminal:

    cd /path/to/matlab/comsol/package/parent/folder
    git clone https://ngreiner@bitbucket.org/ngreiner/fem_smc_ees.git

    cd /path/to/python/neuron/package/parent/folder
    git clone https://ngreiner@bitbucket.org/ngreiner/biophy_smc_ees.git

The Matlab-Comsol package (_fem_smc_ees_) should be added to the search path of your Matlab install.  
The Python-NEURON package (_biophy_smc_ees_) should be added to the import parth of your Python distribution.

The free open-source software FreeCAD (<https://www.freecadweb.org/>) is also required. The results presented in the article were obtained using FreeCAD for macOS v0.17.


INSTRUCTIONS FOR USE
--------------------
Here, _<root>_ denotes the path to the top-level directory of this research material on your system.

---
The scripts in _<root>/scripts/Matlab_Comsol/_ should be executed first.  
They produce the volume conductor model, compute the electric potential distributions for different electrode active sites, generate the sensorimotor circuits populating the volume conductor model, and interpolates the electric potential along the trajectories of the generated neural entities.  
The scripts should be ran directly from the _<root>/scripts/Matlab_Comsol/_ folder, and in the following order:

1. _build_GM_WM_database_from_tracings.m_
2. _build_GM_WM_CSF_dura_fat_dataset.m_
3. _build_3d_coordinates.m_
4. _build_3d_volumes.py_ (from within FreeCAD, see comments inside the script)
5. _build_run_FEM_model.m_ (requires Comsol server to be running and Matlab connected to the server)
6. _build_smcd_1.m_ (requires Comsol server to be running and Matlab connected to the server)
7. _build_smcd_2.m_ (requires Comsol server to be running and Matlab connected to the server)
8. _build_smcd_3.m_ (requires Comsol server to be running and Matlab connected to the server)
9. _build_smcd_4.m_ (requires Comsol server to be running and Matlab connected to the server)

---
Only then can the scripts in _<root>/scripts/Python_NEURON/scripts_building_simulation_results/_ be executed.  
They perform the neurophysical simulations, assessing the recruitment of neural entities to electrical stimulation or to synaptic excitation.  
Their execution should be performed from the directory _<root>/scripts/Python_NEURON/execdir/_, and, prior to their execution, one should have compiled the .mod files located in _<root>/scripts/Python_NEURON/execdir/mod_files/_ using the command `nrnivmodl mod_files` (it's only necessary to run it once).  
The scripts come in two flavours : serial and parallel.  
To execute the serial version of the scripts, simply run

	python ../scripts_building_simulation_results/serial/name_of_script.py

from _<root>/scripts/Python_NEURON/execdir/_.  
To execute the parallel version of the scripts, you will need an MPI library such as Open MPI (<https://www.open-mpi.org/>) and the Python package _mpi4py_. The execution is then performed using the idiom

	mpiexec -np X python ../scripts_building_simulation_results/parallel/name_of_script.py

where X denotes the number of processing units you want to use.  
Some scripts require that other scripts be ran first. Here is a possible running order (refer to the scripts if you want to find out the dependency relationships):

1. _Ia_1.py_
2. _II.py_
3. _DCcB.py_
4. _CST.py_
5. _ST.py_
6. _MN_Eext.py_
7. _Ia_2.py_
8. _Ia_recruitment_localization.py_
9. _Ia_3.py_
10. _EPSP_analysis.py_
11. _MN_StatSynFromNFib.py_
12. _find_gmax_nsyn_muscles.py_
13. _MN_StatSynFromIa_1.py_
14. _MN_StatSynFromIa_2.py_

---
The scripts in _<root>/scripts/Python_NEURON/scripts_building_figure_plots/_ can finally be executed. They should also be executed from the _<root>/scripts/Python_NEURON/execdir/_ folder. The order of execution is unimportant.


ASKING HELP
-----------
Please address your questions to Nathan GREINER at: <mailto:nathan.greiner@m4x.org>
