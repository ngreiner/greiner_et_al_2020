# File :  README.txt
#
# Created on : 17 October 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch

The folders MkCa, MkCe, MkLi, MkLo and MkSa each enclose experimental
data obtained from one animal.

For each, data obtained with 3 different electrode active sites
(approximately corresponding to the virtual electrodes elec1, elec4
and elec10 of the volume conductor model) is included.

This data consists in the recruitment curves of 8 muscles :
ABP, BIC, DEL, ECR, EDC, FCR, FDS, and TRI.

For each muscle and each electrode, 11 values corresponding to the
mean recruitment level of the muscle at 11 increasing amplitudes
are given.

The 'Stim.txt' textfile specifies the lowest and highest amplitudes
used in micro-amperes.
