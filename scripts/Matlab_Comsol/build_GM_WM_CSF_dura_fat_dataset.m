%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :   build_GM_WM_CSF_dura_fat_dataset.m
%
% Created on : 28 April 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
%
% Description:
%   This script should be executed after the script
%   'build_GM_WM_coordinates_from_tracings.m'. 
%
%   It builds a series of geometric coordinates textfiles describing
%   the contours of the GM, WM, CSF, dura and fat at specific spinal
%   segment cross-sections complying to specific geometrical and
%   morphological prescriptions.
%
%   It relies on the following resources:
%       
%       - the directory '<root>/simulation_data/Matlab_Comsol/resources/...
%       cross_section_database/geometric_coordinates/' containing textfiles
%       describing cross-sectional contours  of the GM and WM. (These are
%       built by the script 'build_GM_WM_coordinates_from_tracings.m'.)
%
%       - the directory '<root>/simulation_data/Matlab_Comsol/resources/...
%       morpho/' containing the textfiles 'segments_dimension.txt',
%       'spacings_CSF.txt', 'spacings_dura.txt', 'spacings_fat.txt' and
%       'z_cross_sections.txt'.
%
%       - the directory '<root>/simulation_data/Matlab_Comsol/resources/...
%       geom/vertebrae_inner_contours/' containing textfiles describing the
%       inner contours of a series of vertebrae.
%
%   The geometric coordinates textfiles are written in subfolders of the
%   directory '<root>/simulation_data/Matlab_Comsol/resources/...
%   cross_sections/'.
%
%   Note: <root> is the path to the top-level directory of this research
%   material.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to cross-sectional contour database.
CS_database = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol', ...
    'resources', 'cross_section_database', 'geometric_coordinates');

% Specify path to resources directory.
resources_folder = fullfile('..', '..', 'simulation_data', ...
    'Matlab_Comsol', 'resources');

% Derive paths to various folders where to read from/write to.
morpho_folder = fullfile(resources_folder, 'morpho');
geom_folder = fullfile(resources_folder, 'geom');
CS_folder = fullfile(geom_folder, 'cross_sections');
vert_folder = fullfile(geom_folder, 'vertebrae_inner_contours');

% Specify cross-sections.
CS = {'C5'; 'C6'; 'C7'; 'C8'; 'T1'; 'T2'};

% Number of points for resampling imported contours (should be odd).
np = 301;

% Load segments' cross-sections' transversal and saggital widths.
fileID = fopen(fullfile(morpho_folder, 'segments_dimensions.txt'));
seg_dims = textscan(fileID, '%s %f %f');
fclose(fileID);

% Load segments' cross-sections' z-coordinates.
fileID = fopen(fullfile(morpho_folder, 'z_cross_sections.txt'));
tmp = textscan(fileID, '%s %f');
zCS = cellfun(@(x) tmp{2}(strcmp(x, tmp{1})), CS);
fclose(fileID);
    
% Load spacings between CSF and WM.
fileID = fopen(fullfile(morpho_folder, 'spacings_CSF.txt'));
spacings_CSF = textscan(fileID,'%s %f %f %f %f');
fclose(fileID);

% Load spacings between dura and CSF.
fileID = fopen(fullfile(morpho_folder, 'spacings_dura.txt'));
spacings_dura = textscan(fileID,'%s %f %f %f %f');
fclose(fileID);

% Load spacings between fat and dura.
fileID = fopen(fullfile(morpho_folder, 'spacings_fat.txt'));
spacings_fat = textscan(fileID,'%s %f %f %f %f');
fclose(fileID);

% Recover inner contours of vertebrae by scanning the appropriate 
% folder.
IC_names = Utils.scan_files(vert_folder);

% Import all inner contours.
IC = cell(size(IC_names));
zIC = zeros(size(IC_names));    
for i = 1 : size(IC, 1)
    fileID = fopen(fullfile(vert_folder, IC_names{i}));
    IC{i} = textscan(fileID, '%f %f %f');
    fclose(fileID);
    zIC(i) = mean(IC{i}{3});
end

% Process inner contours for CSF and dura.
IC1 = cell(size(IC_names));
for i = 1 : size(IC1, 1)
    C = Geom.contour_from_shuffled_points([IC{i}{1}, IC{i}{2}]);
    C = Geom.symetrize(C, 'convex');
    C = Geom.interparc(np, C(:, 1), C(:, 2), 'linear');
    IC1{i} = C;
end

% Process inner contours for fat.
IC2 = cell(size(IC_names));
for i = 1 : size(IC2, 1)
    C = Geom.contour_from_shuffled_points([IC{i}{1}, IC{i}{2}]);
    C = Geom.symetrize(C, 'minimal');
    C = Geom.interparc(np, C(:, 1), C(:, 2), 'linear');
    IC2{i} = C;
end

% Build GM, WM, CSF, fat and dura contours for each cross-section.
for i = 1 : size(CS, 1)
    
    % Load GM and WM from cross-section database.
    fileID = fopen(fullfile(CS_database, CS{i}, 'GM.txt'));
    GM = textscan(fileID, '%f %f');
    fclose(fileID);
    
    fileID = fopen(fullfile(CS_database, CS{i}, 'WM.txt'));
    WM = textscan(fileID, '%f %f');
    fclose(fileID);
    
    % Scale GM and WM according to specified dimensions.
    WM_curr_trans_width = max(WM{1}) - min(WM{1});
    WM_curr_sag_width = max(WM{2}) - min(WM{2});  
    WM_targ_trans_width = seg_dims{2}(strcmp(seg_dims{1}, CS{i}));
    WM_targ_sag_diam = seg_dims{3}(strcmp(seg_dims{1}, CS{i}));
    GM{1} = GM{1} * WM_targ_trans_width / WM_curr_trans_width;
    GM{2} = GM{2} * WM_targ_sag_diam / WM_curr_sag_width;
    WM{1} = WM{1} * WM_targ_trans_width / WM_curr_trans_width;
    WM{2} = WM{2} * WM_targ_sag_diam / WM_curr_sag_width;
    
    % Resample curves.
    GM = Geom.interparc(np, GM{1}, GM{2}, 'linear');
    WM = Geom.interparc(np, WM{1}, WM{2}, 'linear');
    
    % Write textfiles.
    if ~ exist(fullfile(CS_folder, CS{i}), 'dir')
        mkdir(fullfile(CS_folder, CS{i}));
    end
    
    fileID = fopen(fullfile(CS_folder, CS{i}, 'GM.txt'), 'w');
    fprintf(fileID,'%.14f %.14f\n', GM');
    fclose(fileID);
    
    fileID = fopen(fullfile(CS_folder, CS{i}, 'WM.txt'), 'w');
    fprintf(fileID, '%.14f %.14f\n', WM');
    fclose(fileID);
        
    % Build CSF, fat and dura.
        
    % Recover CS z-coordinate and find appropriate IC contours for
    % interpolation.
    z = zCS(i);
    if all(z >= zIC)
        ICCS1 = IC1{1};
        ICCS2 = IC2{1};
    elseif all(z <= zIC)
        ICCS1 = IC1{end};
        ICCS2 = IC2{end};
    elseif any(z == zIC)
        ICCS1 = IC1{z == zIC};
        ICCS2 = IC2{z == zIC};
    else
        idx_down = find(z > zIC);
        idx_up = find(z < zIC);
        z_down = zIC(idx_down(1));
        z_up = zIC(idx_up(end));
        w = (z_up - z) / (z_up - z_down);
        ICCS1 = w * IC1{idx_down(1)} + (1 - w) * IC1{idx_up(end)};
        ICCS2 = w * IC2{idx_down(1)} + (1 - w) * IC2{idx_up(end)};
    end

    % Make CSF.
    CSF = ICCS1;
    % Scale CSF.
    left_spacing = spacings_CSF{2}(strcmp(spacings_CSF{1}, CS{i}));
    right_spacing = spacings_CSF{3}(strcmp(spacings_CSF{1}, CS{i}));
    ventral_spacing = spacings_CSF{4}(strcmp(spacings_CSF{1}, CS{i}));
    dorsal_spacing = spacings_CSF{5}(strcmp(spacings_CSF{1}, CS{i}));
    xmin_WM = min(WM(:, 1));
    xmax_WM = max(WM(:, 1));
    ymin_WM = min(WM(:, 2));
    ymax_WM = max(WM(:, 2));
    xmin_CSF = min(CSF(:, 1));
    xmax_CSF = max(CSF(:, 1));
    ymin_CSF = min(CSF(:, 2));
    ymax_CSF = max(CSF(:, 2));
    sf_trans = (xmax_WM - xmin_WM + left_spacing + right_spacing) / (xmax_CSF - xmin_CSF);
    sf_sag = (ymax_WM - ymin_WM + ventral_spacing + dorsal_spacing) / (ymax_CSF - ymin_CSF);
    CSF(:, 1) = (CSF(:, 1) - xmin_CSF) * sf_trans + (xmin_WM - left_spacing);
    CSF(:, 2) = (CSF(:, 2) - ymin_CSF) * sf_sag + (ymin_WM - dorsal_spacing);

    % Make dura.
    dura = zeros(size(CSF));
    % Scale dura.
    left_spacing = spacings_dura{2}(strcmp(spacings_dura{1}, CS{i}));
    right_spacing = spacings_dura{3}(strcmp(spacings_dura{1}, CS{i}));
    ventral_spacing = spacings_dura{4}(strcmp(spacings_dura{1}, CS{i}));
    dorsal_spacing = spacings_dura{5}(strcmp(spacings_dura{1}, CS{i}));
    xmin_CSF = min(CSF(:, 1));
    xmax_CSF = max(CSF(:, 1));
    ymin_CSF = min(CSF(:, 2));
    ymax_CSF = max(CSF(:, 2));
    sf_trans = (xmax_CSF - xmin_CSF + left_spacing + right_spacing) / (xmax_CSF - xmin_CSF);
    sf_sag = (ymax_CSF - ymin_CSF + ventral_spacing + dorsal_spacing) / (ymax_CSF - ymin_CSF);
    dura(:, 1) = (CSF(:, 1) - xmin_CSF) * sf_trans + (xmin_CSF - left_spacing);
    dura(:, 2) = (CSF(:, 2) - ymin_CSF) * sf_sag + (ymin_CSF - dorsal_spacing);

    % Make fat.
    fat = ICCS2;
    % Scale fat.
    left_spacing = spacings_fat{2}(strcmp(spacings_fat{1}, CS{i}));
    right_spacing = spacings_fat{3}(strcmp(spacings_fat{1}, CS{i}));
    ventral_spacing = spacings_fat{4}(strcmp(spacings_fat{1}, CS{i}));
    dorsal_spacing = spacings_fat{5}(strcmp(spacings_fat{1}, CS{i}));
    xmindura = min(dura(:, 1));
    xmaxdura = max(dura(:, 1));
    ymin_dura = min(dura(:, 2));
    ymax_dura = max(dura(:, 2));
    xmin_fat = min(fat(:, 1));
    xmax_fat = max(fat(:, 1));
    ymin_fat = min(fat(:, 2));
    ymax_fat = max(fat(:, 2));
    sf_trans = (xmaxdura - xmindura + left_spacing + right_spacing) / (xmax_fat - xmin_fat);
    sf_sag = (ymax_dura - ymin_dura + ventral_spacing + dorsal_spacing) / (ymax_fat - ymin_fat);
    fat(:, 1) = (fat(:, 1) - xmin_fat) * sf_trans + (xmindura - left_spacing);
    fat(:, 2) = (fat(:, 2) - ymin_fat) * sf_sag + (ymin_dura - dorsal_spacing);
        
    % Write textfiles.
    fileID = fopen(fullfile(CS_folder, CS{i}, 'CSF.txt'), 'w');
    fprintf(fileID,'%.14f %.14f\n', CSF');
    fclose(fileID);

    fileID = fopen(fullfile(CS_folder, CS{i}, 'fat.txt'), 'w');
    fprintf(fileID, '%.14f %.14f\n', fat');
    fclose(fileID);

    fileID = fopen(fullfile(CS_folder, CS{i}, 'dura.txt'), 'w');
    fprintf(fileID, '%.14f %.14f\n', dura');
    fclose(fileID);
    
end
