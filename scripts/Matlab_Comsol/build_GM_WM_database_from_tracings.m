%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  build_GM_WM_coordinates_from_tracings.m
%
% Created on : 19 April 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
%
% Description :
%   This script builds a series of geometric coordinates textfiles
%   describing the contours of the GM and WM of the spinal cord from 
%   a series of digital tracing textfiles previously obtained from scanned
%   pictures of spinal cross-sections using NeuronJ.
%
%   It relies on the following resources:
%
%       - the directory '<root>/simulation_data/Comsol_Matlab/...
%       cross_section_database/tracings/' containing a series of subfolders
%       each named after a spinal cross-section and each containing the
%       files 'Tracing.Out.txt', 'Tracing.In.txt', 'Tracing.Center.txt' and
%       'Tracing.ScaleBar.txt'.
%
%   The geometric coordinates textfiles produced by this script are
%   2-dimensional : the z-coordinate is absent.
%   The textfiles are written in '<root>/simulation_data/Comsol_Matlab/...
%   cross_section_database/geometric_coordinates/'.
%   The contours they describe are clockwise oriented and their first
%   point (which is also the last) is the point with x=0 and y<0.
%
%   Note: <root> is the path to the top-level directory of this research
%   material.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Specify input and output folders.
base_path = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol', ...
    'resources', 'cross_section_database');
input_folder = fullfile(base_path, 'tracings');
output_folder = fullfile(base_path, 'geometric_coordinates');


% Specify cross-sections.
CS = {'C1'; 'C2'; 'C3'; 'C4'; 'C5'; 'C6'; 'C7'; 'C8'; ...
      'T1'; 'T2'; 'T3'; 'T4'; 'T5'; 'T6'};
win_sizes_GM = [ 5;  5;  5;  5;  5;  5; 5; 5; 5; 7; 7; 3; 3; 3];
win_sizes_WM = [40; 40; 40; 40; 40; 10; 5; 5; 5; 5; 5; 2; 2; 2];


% Buld geometric coordinates. Export to textfiles.
for i = 1 : size(CS, 1)
    
    % Load tracings previously obtained with NeuronJ.
    fileID = fopen(fullfile(input_folder, CS{i}, 'Tracing.In.txt'));
    GM = textscan(fileID,'%f %f');
    fclose(fileID);
    
    fileID = fopen(fullfile(input_folder, CS{i}, 'Tracing.Out.txt'));
    WM = textscan(fileID,'%f %f');
    fclose(fileID);
    
    fileID = fopen(fullfile(input_folder, CS{i}, 'Tracing.Center.txt'));
    center = textscan(fileID,'%f %f');
    fclose(fileID);
    
    fileID = fopen(fullfile(input_folder, CS{i}, 'Tracing.ScaleBar.txt'));
    scale_bar = textscan(fileID,'%f %f');
    fclose(fileID);
    
    % Compute scaling factor (in mm/pixel).
    sf = 1 / (scale_bar{1}(2) - scale_bar{1}(1));
    
    % Compute center coordinates (in pixels).
    xc = mean(center{1});
    yc = mean(center{2});
    
    % Shift and scale GM and WM.
    GM{1} = (GM{1} - xc) * sf;
    GM{2} = (GM{2} - yc) * sf;
    WM{1} = (WM{1} - xc) * sf;
    WM{2} = (WM{2} - yc) * sf;
    
    % Close GM and WM.
    GM = [[GM{1}, GM{2}]; [GM{1}(1), GM{2}(1)]];
    WM = [[WM{1}, WM{2}]; [WM{1}(1), WM{2}(1)]];
    
    % Extract righ-half contours.
    GM = Geom.extract_right_half(GM, 'direction', 'counterclock', 'start_at', 'x=0,ymin');
    WM = Geom.extract_right_half(WM, 'direction', 'counterclock', 'start_at', 'x=0,ymin');
    
    % Open extracted right-half contours.
    GM(end, :) = [];
    WM(end, :) = [];
    
    % Smooth them.
    GM = Geom.smooth_2d_curve(GM, win_sizes_GM(i));
    WM = Geom.smooth_2d_curve(WM, win_sizes_WM(i));
    
    % Form symmetric closed-contour by reflecting the GM and WM with
    % respect to the y-axis.
    GM = [Geom.flip_x_coords(GM); flipud(GM(1 : end - 1, :))];
    WM = [Geom.flip_x_coords(WM); flipud(WM(1 : end - 1, :))];
    
    % Write to text files. Make CS folder if needed.
    if ~ exist(fullfile(output_folder, CS{i}), 'dir')
        mkdir(fullfile(output_folder, CS{i}))
    end
    
    fileID = fopen(fullfile(output_folder, CS{i}, 'GM.txt'), 'w');
    fprintf(fileID,'%.14f %.14f\n', GM');
    fclose(fileID);
    
    fileID = fopen(fullfile(output_folder, CS{i}, 'WM.txt'), 'w');
    fprintf(fileID,'%.14f %.14f\n', WM');
    fclose(fileID);
    
    % Plot GM and WM contours.
    scrsz = get(groot,'ScreenSize');
    if mod(i, 4) == 1
        figure('Position', [1, scrsz(4) / 2, scrsz(3) / 2, scrsz(4) / 2])
        subplot(2, 2, 1)
    else
        subplot(2, 2, rem(i - 1, 4) + 1)
    end
    plot(GM(:, 1), GM(:, 2), WM(:, 1), WM(:, 2));
    title(CS{i})
    axis([-6 6 -5 5])
    
end
