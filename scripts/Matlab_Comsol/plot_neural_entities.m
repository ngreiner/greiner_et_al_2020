%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  plot_neural_entities.m
%
% Created on : 17 October 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
% Description:
%   This script should be executed after the scripts
%   'build_smcd_*.m'.
%
%   It plots the trajectories of neural entities relative to the spinal
%   cord.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to FEM dataset directory.
topdir = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol');

% Instantiate sensorimotor circuit model.
smcd_name = 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs';
smcd = SMCDModel('topdir', topdir, 'name', smcd_name);
smcd.load();

% Plot neural entity trajectories.
smcd.plot('MNs', 'MN_gidx', 1);
smcd.plot('Ias', 'Ia_gidx', 1);
smcd.plot('IIs', 'II_gidx', 1);
smcd.plot('DCcBs', 'DCcB_gidx', 1);
smcd.plot('CSTs', 'CST_gidx', 1);
smcd.plot('STs', 'ST_gidx', 1);
