# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_fig_2d.py
#
# Created on : 30 January 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the script 'Ia_recruitment_localization.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.utils.Plotter import Plotter
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils import maths


# Script parameters.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C5_C6_C7_C8_T1_Ias')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C5_C6_C7_C8_T1_Ias')
smc = SMC(dir_data=dir_data, dir_res=dir_res)
combi = 'elec10'
amps = -25.0 * np.linspace(1, 60, num=60)
pop_size = 10000

# fibers of interest.
proxigidxs = smc.get_Ia_gidxs(seg='C6')
nproxi = len(proxigidxs)
dist1gidxs = smc.get_Ia_gidxs(seg='C5')
ndist1 = len(dist1gidxs)
dist2gidxs = smc.get_Ia_gidxs(seg='C7')
ndist2 = len(dist2gidxs)

# Choose mode.
# 1 = run new statistical analysis using a Boostrapping approach.
# 2 = load previous results and rebuild plots.
mode = 1
save = True

# Folder where to save results: this should be the same as that
# specified in the script Ia_recruitment_localization.py, and it
# should contain the file fig_2d_fibers_records.pck, created by the
# samce script.
folder = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')

# Perform new statistical analysis if mode=1.
if mode == 1:

    fibers_records = pickle.load(open(os.path.join(folder, 'fig_2d_fibers_records.pck'), 'rb'))

    # Bootstrap proxi results for statistics.
    proxi_bstrap = Bootstrap(pop_size=pop_size)
    proxi_bstrap.set_data_sequence(proxigidxs)
    proxi_bstrap.gen_pop()
    proxi_DR_recr = np.zeros(amps.size)
    proxi_DR_std = np.zeros(amps.size)
    proxi_DC_recr = np.zeros(amps.size)
    proxi_DC_std = np.zeros(amps.size)

    # Bootstrap dist1 results for statistics.
    dist1_bstrap = Bootstrap(pop_size=pop_size)
    dist1_bstrap.set_data_sequence(dist1gidxs)
    dist1_bstrap.gen_pop()
    dist1_DR_recr = np.zeros(amps.size)
    dist1_DR_std = np.zeros(amps.size)
    dist1_DC_recr = np.zeros(amps.size)
    dist1_DC_std = np.zeros(amps.size)

    # Bootstrap dist2 results for statistics.
    dist2_bstrap = Bootstrap(pop_size=pop_size)
    dist2_bstrap.set_data_sequence(dist2gidxs)
    dist2_bstrap.gen_pop()
    dist2_DR_recr = np.zeros(amps.size)
    dist2_DR_std = np.zeros(amps.size)
    dist2_DC_recr = np.zeros(amps.size)
    dist2_DC_std = np.zeros(amps.size)

    # Store total recruitment statistics.
    total_recr = np.zeros(amps.size)
    total_std = np.zeros(amps.size)

    # Go Boostrap.
    for i, amp in enumerate(amps):

        print('amp: {:f} ...'.format(amp))

        tmp_proxi_DR = np.zeros(pop_size)
        tmp_proxi_DC = np.zeros(pop_size)
        tmp_dist1_DR = np.zeros(pop_size)
        tmp_dist1_DC = np.zeros(pop_size)
        tmp_dist2_DR = np.zeros(pop_size)
        tmp_dist2_DC = np.zeros(pop_size)

        for j in range(pop_size):

            for k, gidx in enumerate(proxi_bstrap.pop[:, j]):
                if fibers_records[gidx][amp]['recr'] == 1.0:
                    if fibers_records[gidx][amp]['idx'] == 0:
                        tmp_proxi_DR[j] += 1.0
                    elif fibers_records[gidx][amp]['label'] == 'DRbranch':
                        tmp_proxi_DR[j] += 1.0
                    else:
                        tmp_proxi_DC[j] += 1.0
                elif i > 0:
                    fibers_records[gidx][amp] = fibers_records[gidx][amps[i - 1]]
                    if fibers_records[gidx][amp]['recr'] == 1.0:
                        if fibers_records[gidx][amp]['idx'] == 0:
                            tmp_proxi_DR[j] += 1.0
                        elif fibers_records[gidx][amp]['label'] == 'DRbranch':
                            tmp_proxi_DR[j] += 1.0
                        else:
                            tmp_proxi_DC[j] += 1.0

            for k, gidx in enumerate(dist1_bstrap.pop[:, j]):
                if fibers_records[gidx][amp]['recr'] == 1.0:
                    if fibers_records[gidx][amp]['idx'] == 0:
                        tmp_dist1_DR[j] += 1.0
                    elif fibers_records[gidx][amp]['label'] == 'DRbranch':
                        tmp_dist1_DR[j] += 1.0
                    else:
                        tmp_dist1_DC[j] += 1.0
                elif i > 0:
                    fibers_records[gidx][amp] = fibers_records[gidx][amps[i - 1]]
                    if fibers_records[gidx][amp]['recr'] == 1.0:
                        if fibers_records[gidx][amp]['idx'] == 0:
                            tmp_dist1_DR[j] += 1.0
                        elif fibers_records[gidx][amp]['label'] == 'DRbranch':
                            tmp_dist1_DR[j] += 1.0
                        else:
                            tmp_dist1_DC[j] += 1.0

            for k, gidx in enumerate(dist2_bstrap.pop[:, j]):
                if fibers_records[gidx][amp]['recr'] == 1.0:
                    if fibers_records[gidx][amp]['idx'] == 0:
                        tmp_dist2_DR[j] += 1.0
                    elif fibers_records[gidx][amp]['label'] == 'DRbranch':
                        tmp_dist2_DR[j] += 1.0
                    else:
                        tmp_dist2_DC[j] += 1.0
                elif i > 0:
                    fibers_records[gidx][amp] = fibers_records[gidx][amps[i - 1]]
                    if fibers_records[gidx][amp]['recr'] == 1.0:
                        if fibers_records[gidx][amp]['idx'] == 0:
                            tmp_dist2_DR[j] += 1.0
                        elif fibers_records[gidx][amp]['label'] == 'DRbranch':
                            tmp_dist2_DR[j] += 1.0
                        else:
                            tmp_dist2_DC[j] += 1.0

        # Form total recruitment.
        tmp_total = tmp_proxi_DR + tmp_proxi_DC + tmp_dist1_DR + tmp_dist1_DC + tmp_dist2_DR + tmp_dist2_DC

        # Uncomment next seven lines if you want to obtain the proportional recruitment instead of absolute.
        tmp_proxi_DR = tmp_proxi_DR * (100. / nproxi)
        tmp_proxi_DC = tmp_proxi_DC * (100. / nproxi)
        tmp_dist1_DR = tmp_dist1_DR * (100. / ndist1)
        tmp_dist1_DC = tmp_dist1_DC * (100. / ndist1)
        tmp_dist2_DR = tmp_dist2_DR * (100. / ndist2)
        tmp_dist2_DC = tmp_dist2_DC * (100. / ndist2)
        tmp_total = tmp_total * 100. / (nproxi + ndist1 + ndist2)

        proxi_DR_recr[i] = np.mean(tmp_proxi_DR)
        proxi_DR_std[i] = np.std(tmp_proxi_DR)
        proxi_DC_recr[i] = np.mean(tmp_proxi_DC)
        proxi_DC_std[i] = np.std(tmp_proxi_DC)

        dist1_DR_recr[i] = np.mean(tmp_dist1_DR)
        dist1_DR_std[i] = np.std(tmp_dist1_DR)
        dist1_DC_recr[i] = np.mean(tmp_dist1_DC)
        dist1_DC_std[i] = np.std(tmp_dist1_DC)

        dist2_DR_recr[i] = np.mean(tmp_dist2_DR)
        dist2_DR_std[i] = np.std(tmp_dist2_DR)
        dist2_DC_recr[i] = np.mean(tmp_dist2_DC)
        dist2_DC_std[i] = np.std(tmp_dist2_DC)

        total_recr[i] = np.mean(tmp_total)
        total_std[i] = np.std(tmp_total)

    # Find activation amplitude thresholds.
    threshold = 0.1 * (nproxi + ndist1 + ndist2)
    try:
        iIxxh = np.flatnonzero(total_recr >= threshold)[0]
        iIxxl = np.flatnonzero(total_recr <= threshold)[-1]
        Ixxh = amps[iIxxh]
        Ixxl = amps[iIxxl]
        Axxh = total_recr[iIxxh]
        Axxl = total_recr[iIxxl]
        if Ixxh == Ixxl:
            Ixx0 = Ixxl
        elif Axxh == Axxl:
            Ixx0 = Ixxl
        else:
            Ixx0 = Ixxl + (Ixxh - Ixxl) / (Axxh - Axxl) * (threshold - Axxl)
    except:
            Ixx0 = amps[-1]  # amplitudes are negative until here

    # Normalize amplitudes.
    amps = amps / Ixx0

    if save:
        res = dict()
        res['amps'] = amps
        res['proxi_DR_recr'] = proxi_DR_recr
        res['proxi_DC_recr'] = proxi_DC_recr
        res['proxi_DR_std'] = proxi_DR_std
        res['proxi_DC_std'] = proxi_DC_std
        res['dist1_DR_recr'] = dist1_DR_recr
        res['dist1_DC_recr'] = dist1_DC_recr
        res['dist1_DR_std'] = dist1_DR_std
        res['dist1_DC_std'] = dist1_DC_std
        res['dist2_DR_recr'] = dist2_DR_recr
        res['dist2_DC_recr'] = dist2_DC_recr
        res['dist2_DR_std'] = dist2_DR_std
        res['dist2_DC_std'] = dist2_DC_std
        res['total_recr'] = total_recr
        res['total_std'] = total_std
        pickle.dump(res, open(os.path.join(folder, 'fig_2d.pck'), 'wb'))

# Otherwise, import previous results and build new plots.
else:
    res = pickle.load(open(os.path.join(folder, 'fig_2d.pck'), 'rb'))
    amps = res['amps']
    proxi_DR_recr = res['proxi_DR_recr']
    proxi_DC_recr = res['proxi_DC_recr']
    proxi_DR_std = res['proxi_DR_std']
    proxi_DC_std = res['proxi_DC_std']
    dist1_DR_recr = res['dist1_DR_recr']
    dist1_DC_recr = res['dist1_DC_recr']
    dist1_DR_std = res['dist1_DR_std']
    dist1_DC_std = res['dist1_DC_std']
    dist2_DR_recr = res['dist2_DR_recr']
    dist2_DC_recr = res['dist2_DC_recr']
    dist2_DR_std = res['dist2_DR_std']
    dist2_DC_std = res['dist2_DC_std']
    total_recr = res['total_recr']
    total_std = res['total_std']

# Smooth curves.
proxi_DR_recr = maths.running_mean(proxi_DR_recr)
proxi_DC_recr = maths.running_mean(proxi_DC_recr)
proxi_DR_std = maths.running_mean(proxi_DR_std)
proxi_DC_std = maths.running_mean(proxi_DC_std)
dist1_DR_recr = maths.running_mean(dist1_DR_recr)
dist1_DC_recr = maths.running_mean(dist1_DC_recr)
dist1_DR_std = maths.running_mean(dist1_DR_std)
dist1_DC_std = maths.running_mean(dist1_DC_std)
dist2_DR_recr = maths.running_mean(dist2_DR_recr)
dist2_DC_recr = maths.running_mean(dist2_DC_recr)
dist2_DR_std = maths.running_mean(dist2_DR_std)
dist2_DC_std = maths.running_mean(dist2_DC_std)
total_recr = maths.running_mean(total_recr)
total_std = maths.running_mean(total_std)

# Plotting section.
plotter = Plotter(subplot_width=4, subplot_height=4)
fig, axs = plotter.build_figure(1)

pp = dict()
pp['ax_title'] = 'Ia-fibers recruitment | elec. lat. C6'
pp['xlabel'] = 'normalized amplitude'
pp['ylabel'] = 'fiber recruitment (%)'
pp['labels'] = ['C5 (DR)', 'C6 (DR)', 'C7 (DR)',
                'C5 (DC)', 'C6 (DC)', 'C7 (DC)']
pp['colors'] = plotter.get_colors('inferno', 3, lthresh=0.1, hthresh=0.5) * 2
pp['styles'] = ['--', '--', '--', ':', ':', ':']
pp['ylim'] = [0.0, 100.0]
maxamp = max(amps)
pp['xlim'] = [0.0, maxamp]
plotter.plot_bounded_lines(axs[0], amps, dist1_DR_recr, dist1_DR_std,
                                   amps, proxi_DR_recr, proxi_DR_std,
                                   amps, dist2_DR_recr, dist2_DR_std,
                                   amps, dist1_DC_recr, dist1_DC_std,
                                   amps, proxi_DC_recr, proxi_DC_std,
                                   amps, dist2_DC_recr, dist2_DC_std, **pp)

# Legends.
fig.legend(axs[0].lines, pp['labels'], ncol=1, loc='center right', bbox_to_anchor=(1.0, 0.5))

plotter.tight_layout(pad=4)

if save:
    plotter.savefig(os.path.join(folder, 'fig_2d.svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig(os.path.join(folder, 'fig_2d.png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
