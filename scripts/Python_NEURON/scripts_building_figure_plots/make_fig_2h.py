# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_fig_2h.py
#
# Created on : 1 July 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the script 'Ia_2.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils.Plotter import Plotter


# Choose and load sensorimotor circuit dataset.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C5_C6_C7_C8_T1_Ias')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C5_C6_C7_C8_T1_Ias')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose entity.
# Choose electrodes.
# Choose stimulation amplitudes.
entity = 'Ia'
combis = ['elec13', 'elec14', 'elec10', 'elec11', 'elec7', 'elec8', 'elec4', 'elec5', 'elec1', 'elec2']
targ_segs = ['C5', 'C5', 'C6', 'C6', 'C7', 'C7', 'C8', 'C8', 'T1', 'T1']
amps = -25.0 * np.linspace(1, 80, num=80)
pop_size = 10000
stim_type = 'eext'

# Basename for figure/text/binary files.
folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name_base = 'fig_2h'
file_path_base = os.path.join(folder_path, file_name_base)

# Choose mode:
# 1. compute new recruitment curves and build plots (new results can be saved or not)
# 2. import previous recruitment curves and build plots
mode = 1
save = True

# Compute new recruitment curves.
if mode == 1:

    segs = smc.segs
    segs_gidxs = [smc.get_gidxs(entity, seg=seg) for seg in segs]

    # Load entity records and build dictionnary to cache the retrieved recruitment values.
    records = dict()
    recrs = dict()
    for gidxs in segs_gidxs:
        for gidx in gidxs:
            records[gidx] = smc.load_records(entity, gidx=gidx)
            recrs[gidx] = dict()
            for combi in combis:
                recrs[gidx][combi] = dict()

    # Compute recruitment curves for every combi using a Bootstrapping approach.
    combis_specifs = dict()
    combis_stds = dict()

    for combi, targ_seg in zip(combis, targ_segs):

        print('combi: {:} | targeted segment: {:}'.format(combi, targ_seg))

        targ_gidxs = segs_gidxs[segs.index(targ_seg)]
        targ_bstrap = Bootstrap(pop_size=pop_size)
        targ_bstrap.set_data_sequence(targ_gidxs)
        targ_bstrap.gen_pop()

        ntarg_gidxs = []
        for s, seg in enumerate(segs):
            if seg != targ_seg:
                ntarg_gidxs += segs_gidxs[s]
        ntarg_bstrap = Bootstrap(pop_size=pop_size)
        ntarg_bstrap.set_data_sequence(ntarg_gidxs)
        ntarg_bstrap.gen_pop()

        combi_specifs = np.zeros(pop_size)

        for j in range(pop_size):

            print('Bootstrap sample #{:d} ...'.format(j))

            tmp = 0.0

            for i, amp in enumerate(amps):

                targ_rec = 0.0
                for k, gidx in enumerate(targ_bstrap.pop[:, j]):
                    try:
                        targ_rec += recrs[gidx][combi][amp]
                    except:
                        record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)
                                                   & (records[gidx].stim_type == stim_type)]
                        recrs[gidx][combi][amp] = 1.0 if record['recr'].any() else 0.0
                        targ_rec += recrs[gidx][combi][amp]

                ntarg_rec = 0.0
                for k, gidx in enumerate(ntarg_bstrap.pop[:, j]):
                    try:
                        ntarg_rec += recrs[gidx][combi][amp]
                    except:
                        record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)
                                                   & (records[gidx].stim_type == stim_type)]
                        recrs[gidx][combi][amp] = 1.0 if record['recr'].any() else 0.0
                        ntarg_rec += recrs[gidx][combi][amp]

                tmp = max(tmp, targ_rec / len(targ_gidxs) - ntarg_rec / len(ntarg_gidxs))

            combi_specifs[j] = tmp

        combis_specifs[combi] = np.mean(combi_specifs)
        combis_stds[combi] = np.std(combi_specifs)

    # Save results.
    if save:

        # Write data to textfile.
        text = ''
        text += 'Amplitudes\n'
        text += ' '.join(['{:.1f}'.format(amp) for amp in amps])
        text += '\n'
        text += 'Specificities\n'
        for combi in combis:
            text += '{} : {:.4f} +/- {:.4f}\n'.format(combi, combis_specifs[combi], combis_stds[combi])
            text += '\n'
        with open('{}.txt'.format(file_path_base), 'w') as fileID:
            fileID.write(text)

        # Save binary results.
        res = dict(combis_specifs=combis_specifs, combis_stds=combis_stds, combis=combis, amps=amps)
        pickle.dump(res, open('{}.pck'.format(file_path_base), 'wb'))


# Or import previous results.
else:
    res = pickle.load(open('{}.pck'.format(file_path_base), 'rb'))
    combis_specifs = res['combis_specifs']
    combis_stds = res['combis_stds']
    combis = res['combis']
    amps = res['amps']


# Plotting section.

# Define global plotting parameters.
pp = dict()
pp['ylabel'] = 'selectivity index'
pp['with_xtick'] = False
pp['with_xaxis'] = False
pp['cap_size'] = 5.0
pp['cap_thick'] = 1.0
colors = Plotter.get_colors('inferno', 5, lthresh=0.1, hthresh=0.9)
pp['colors'] = [colors[int(i / 2)] for i in range(len(combis))]
pp['alpha_fills'] = [1.0, 0.5] * 5
pp['ylim'] = [0.0, 1.0]

# Create figure.
plotter = Plotter()
fig, axs = plotter.build_figure(1, ncol=1)

# Plot thresholds.
plotter.plot_bars_with_errors(axs[0],
                               1, combis_specifs[combis[0]], combis_stds[combis[0]],
                               2, combis_specifs[combis[1]], combis_stds[combis[1]],
                               4, combis_specifs[combis[2]], combis_stds[combis[2]],
                               5, combis_specifs[combis[3]], combis_stds[combis[3]],
                               7, combis_specifs[combis[4]], combis_stds[combis[4]],
                               8, combis_specifs[combis[5]], combis_stds[combis[5]],
                              10, combis_specifs[combis[6]], combis_stds[combis[6]],
                              11, combis_specifs[combis[7]], combis_stds[combis[7]],
                              13, combis_specifs[combis[8]], combis_stds[combis[8]],
                              14, combis_specifs[combis[9]], combis_stds[combis[9]],
                              **pp)

# Legends.
fig.legend(axs[0].patches[::2] + axs[0].patches[1::2], [x + ' (lat.)' for x in targ_segs[::2]] + [x + ' (mid.)' for x in targ_segs[::2]],
        ncol=1, loc='center left', bbox_to_anchor=(1.0, 0.5))

plotter.tight_layout(pad=2)

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
