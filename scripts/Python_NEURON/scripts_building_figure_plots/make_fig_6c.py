# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_fig_6c.py
#
# Created on : 1 February 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the script 'MN_StatSynFromNFib.py'.
#
#############################################################################


import os
import pickle
import numpy as np

from smcees.neural_entities.SMC import SMC
from smcees.utils.utilities import combi_2_str
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils.Plotter import Plotter
from smcees.utils import maths


# Choose and load sensorimotor circuit dataset.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_MNs_nogeom')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_MNs_nogeom')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose entity.
# Choose combis.
# Choose stimulation amplitudes.
entity = 'MN'
combis = [dict(p_connec=0.3, gmax=10.00),
          dict(p_connec=0.6, gmax=05.00),
          dict(p_connec=0.9, gmax=03.30),
          dict(p_connec=0.3, gmax=15.00),
          dict(p_connec=0.6, gmax=07.50),
          dict(p_connec=0.9, gmax=05.00),
          dict(p_connec=0.3, gmax=22.50),
          dict(p_connec=0.6, gmax=12.25),
          dict(p_connec=0.9, gmax=07.50)]
combis = [combi_2_str(combi) for combi in combis]
amps = 5 * np.linspace(1, 40, num=40, dtype=int)
pop_size = 10000
stim_type = 'stat_syn_from_nfib'

# Basename for figure/text/binary files.
folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name_base = 'fig_6c'
file_path_base = os.path.join(folder_path, file_name_base)

# Choose mode:
# 1. compute new recruitment curves and build plots (new results can be saved or not)
# 2. import previous recruitment curves and build plots
mode = 1
save = True

# Compute new recruitment curves.
if mode == 1:

    gidxs = smc.get_gidxs(entity)

    # Load entity records and build dictionnary to cache the retrieved recruitment values.
    records = dict()
    recrs = dict()
    for gidx in gidxs:
        records[gidx] = smc.load_records(entity, gidx=gidx)
        recrs[gidx] = dict()
        for combi in combis:
            recrs[gidx][combi] = dict()

    # Compute recruitment curves for every combi using a Bootstrapping approach.
    combis_recs = list()
    combis_stds = list()

    for combi in combis:

        recs = np.zeros(amps.size)
        stds = np.zeros(amps.size)

        bstrap = Bootstrap(pop_size=pop_size)
        bstrap.set_data_sequence(gidxs)
        bstrap.gen_pop()

        for i, amp in enumerate(amps):

            print('{} | {:f}'.format(combi, amp))

            amp_recs = np.zeros((len(gidxs), pop_size))
            amp_stds = np.zeros((len(gidxs), pop_size))

            for j in range(pop_size):

                # print 'Bootstrap sample: {:d}'.format(j)

                for k, gidx in enumerate(bstrap.pop[:, j]):
                    try:
                        amp_recs[k, j] = recrs[gidx][combi][amp]
                    except:
                        record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)
                                                   & (records[gidx].stim_type == stim_type)]
                        recrs[gidx][combi][amp] = 1.0 if record['recr'].any() else 0.0
                        amp_recs[k, j] = recrs[gidx][combi][amp]

            amp_recs = np.mean(amp_recs, axis=0)
            recs[i] = np.mean(amp_recs) * 100
            stds[i] = np.std(amp_recs) * 100

        combis_recs.append(recs)
        combis_stds.append(stds)

    # Save results.
    if save:

        # Write data to textfile.
        text = ''
        text += 'Amplitudes\n'
        text += ' '.join(['{:.1f}'.format(amp) for amp in amps])
        text += '\n'
        text += 'Recruitment Curves\n'
        for combi, recs, stds in zip(combis, combis_recs, combis_stds):
            text += '{}\n'.format(combi)
            text += 'Normalized Mean recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in recs])
            text += '\n'
            text += 'Normalized STDS recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in stds])
            text += '\n\n'
        with open('{}.txt'.format(file_path_base), 'w') as fileID:
            fileID.write(text)

        # Save binary results.
        res = dict(combis_recs=combis_recs, combis_stds=combis_stds, combis=combis, amps=amps)
        pickle.dump(res, open('{}.pck'.format(file_path_base), 'wb'))


# Or import previous results.
else:
    res = pickle.load(open('{}.pck'.format(file_path_base), 'rb'))
    combis_recs = res['combis_recs']
    combis_stds = res['combis_stds']
    combis = res['combis']
    amps = res['amps']


# Smooth curves.
for i in range(len(combis)):
    combis_recs[i] = maths.running_mean(combis_recs[i])
    combis_stds[i] = maths.running_mean(combis_stds[i])


# Plotting section.

# Define global plotting parameters.
pp = dict()
pp['xlabel'] = 'activated fibers (#)'
pp['ylabel'] = 'recruitment (%)'
pp['labels'] = combis
pp['colors'] = Plotter.get_colors('greys', 3, lthresh=0.4, hthresh=1.0) * 3
pp['styles'] = ['-'] * 3 + ['--'] * 3 + [':'] * 3
pp['ylim'] = [0.0, 100.0]
pp['xlim'] = [0.0, 200.0]

# Create figure with subplots.
plotter = Plotter()
fig, axs = plotter.build_figure(1)

# Plot recruitment curves.
plist = []
for j in range(len(combis)):
    plist.append(amps)
    plist.append(combis_recs[j])
    plist.append(combis_stds[j])
plotter.plot_bounded_lines(axs[0], *plist, **pp)

# Legends.
fig.legend(axs[0].lines, combis, ncol=1, loc='center left', bbox_to_anchor=(1.0, 0.5))

plotter.tight_layout(pad=2)

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
