# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_fig_6d.py
#
# Created on : 1 February 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the script 'Ia_3.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils.Plotter import Plotter
from smcees.utils import maths


# Choose and load sensorimotor circuit dataset.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_Ias_MNs_8muscs_5segs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_Ias_MNs_8muscs_5segs')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose entity.
# Choose electrodes.
# Choose stimulation amplitudes.
entity = 'Ia'
combi = 'elec10'
amps = - 25.0 * np.linspace(1, 80, num=80)
pop_size = 10000
stim_type = 'eext'

# Choose mode:
# 1. compute new recruitment curves and build plots (new results can be saved or not)
# 2. import previous recruitment curves and build plots
mode = 1
save = True

# Basename for figure/text/binary files.
folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name_base = 'fig_6d'
file_path_base = os.path.join(folder_path, file_name_base)

# Compute new recruitment curves.
if mode == 1:

    muscs = ['DEL', 'BIC', 'TRI', 'EDC', 'ECR', 'FDS', 'FCR', 'ABP']
    muscs_gidxs = [smc.get_gidxs(entity, musc=musc) for musc in muscs]

    # Compute recruitment curves for every muscle using a Bootstrapping approach.
    muscs_recs = list()
    muscs_stds = list()

    for m, musc in enumerate(muscs):

        gidxs = muscs_gidxs[m]
        musc_recs = np.zeros(amps.size)
        musc_stds = np.zeros(amps.size)

        records = dict()
        recrs = dict()
        for gidx in gidxs:
            records[gidx] = smc.load_records(entity, gidx=gidx)
            recrs[gidx] = dict()

        bstrap = Bootstrap(pop_size=pop_size)
        bstrap.set_data_sequence(gidxs)
        bstrap.gen_pop()

        for i, amp in enumerate(amps):

            print('{} | {:f}'.format(musc, amp))

            amp_recs = np.zeros((len(gidxs), pop_size))
            amp_stds = np.zeros((len(gidxs), pop_size))

            for j in range(pop_size):

                for k, gidx in enumerate(bstrap.pop[:, j]):
                    try:
                        amp_recs[k, j] = recrs[gidx][amp]
                    except:
                        record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)
                                                   & (records[gidx].stim_type == stim_type)]
                        recrs[gidx][amp] = 1.0 if record['recr'].any() else 0.0
                        amp_recs[k, j] = recrs[gidx][amp]

            amp_recs = np.mean(amp_recs, axis=0) * 100

            musc_recs[i] = np.mean(amp_recs)
            musc_stds[i] = np.std(amp_recs)

        muscs_recs.append(musc_recs)
        muscs_stds.append(musc_stds)

    # Find activation amplitude thresholds.
    ratio = 0.1
    thresholds = []
    for recs, gidxs in zip(muscs_recs, muscs_gidxs):
        try:
            iIxxh = np.flatnonzero(recs / float(len(gidxs)) >= ratio)[0]
            iIxxl = np.flatnonzero(recs / float(len(gidxs)) <= ratio)[-1]
            Ixxh = amps[iIxxh]
            Ixxl = amps[iIxxl]
            Axxh = musc_recs[iIxxh]
            Axxl = musc_recs[iIxxl]
            if Ixxh == Ixxl:
                Ixx = Ixxl
            elif Axxh == Axxl:
                Ixx = Ixxl
            else:
                Ixx = Ixxl + (Ixxh - Ixxl) / (Axxh - Axxl) * (ratio - Axxl)
        except:
            Ixx = amps[-1]
        thresholds.append(Ixx)
    Ixx0 = max(thresholds)  # amplitudes are negative until here

    # Normalize amplitudes.
    amps = amps / Ixx0

    # Save results.
    if save:

        # Write data to textfile.
        text = ''
        text += 'Amplitudes\n'
        text += ' '.join(['{:.1f}'.format(amp) for amp in amps])
        text += '\n'
        text += 'Recruitment Curves\n'
        for musc, recs, stds in zip(muscs, muscs_recs, muscs_stds):
            text += '{}\n'.format(muscs)
            text += 'Normalized Mean recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in recs])
            text += '\n'
            text += 'Normalized STDS recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in stds])
            text += '\n'
        with open('{}.txt'.format(file_path_base), 'w') as fileID:
            fileID.write(text)

        # Save binary results.
        res = dict(muscs=muscs, muscs_recs=muscs_recs, muscs_stds=muscs_stds, amps=amps)
        pickle.dump(res, open('{}.pck'.format(file_path_base), 'wb'))


# Or import previous results.
else:
    res = pickle.load(open('{}.pck'.format(file_path_base), 'rb'))
    muscs = res['muscs']
    muscs_recs = res['muscs_recs']
    muscs_stds = res['muscs_stds']
    amps = res['amps']


# Smooth curves.
for i in range(len(muscs)):
    muscs_recs[i] = maths.running_mean(muscs_recs[i])
    muscs_stds[i] = maths.running_mean(muscs_stds[i])


# Plotting section.

# Define global plotting parameters.
pp = dict()
pp['xlabel'] = 'normalized amplitude'
pp['ylabel'] = 'recruitment (% entities)'
pp['tight_layout'] = True
pp['labels'] = muscs
pp['colors'] = [(0.031372550874948502, 0.11372549086809158, 0.34509804844856262),
                (0.141361021469621100, 0.25623991208917951, 0.60530567519805012),
                (0.501960813999176030, 0.00000000000000000, 0.14901961386203766),
                (0.992095348414252730, 0.54906577035492543, 0.23418685215360979),
                (0.886689741471234470, 0.09956170837668811, 0.11072664462468203),
                (0.252687442302703850, 0.71144945972106033, 0.76838140557794010),
                (0.120261440674463910, 0.50196080406506860, 0.72156864404678345),
                (0.586082300251605460, 0.58608230025160546, 0.58608230025160546)]
pp['ylim'] = [0.0, 100.0]
pp['xlim'] = [0.0, max(amps)]

# Create figure with subplots.
plotter = Plotter(**pp)
fig, axs = plotter.build_figure(1)

# Plot muscle recruitment curves.
plist = []
for i in range(len(muscs)):
    plist.append(amps)
    plist.append(muscs_recs[i])
    plist.append(muscs_stds[i])
plotter.plot_bounded_lines(axs[0], *plist, **pp)

# Legends.
fig.legend(axs[0].lines, muscs, ncol=1, loc='center right', bbox_to_anchor=(1.0, 0.5))

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
