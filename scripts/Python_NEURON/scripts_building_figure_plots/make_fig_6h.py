# -*- coding: utf-8 -*


#############################################################################
# Script : .../make_fig_6h.py
#
# Created on : 18 May 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the scripts 'MN_StatSynFromIa_1.py'
#   and 'MN_StatSynFromIa_2.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.utils import maths
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils.Plotter import Plotter


# Comparison parameters.
xthresh = 0.1
xsat = 0.9
pop_size = 200
elecs = ['elec10', 'elec4', 'elec1']
muscs = ['DEL', 'BIC', 'TRI', 'EDC', 'ECR', 'FDS', 'FCR', 'ABP']
cost_fun = lambda x: np.sqrt(np.mean(np.square(x)))


# Choose mode.
# 1: Build new simulated recruitments dictionnary and new statistics.
# 2: Import previous dictionnary and build new statistics.
# 3: Import previous dictionnary and previous statistics.
mode = 1
save = True
dir_export = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
export_file_path_base = os.path.join(dir_export, 'fig_6h')


# Load experimental recruitment curves.
dir_import = os.path.join('..', '..', '..', 'experimental_data', 'for_comparison_with_simulations')
animals = ['MkCa', 'MkCe', 'MkLi', 'MkLo', 'MkSa']

# Format experimental recruitments.
exp_recrs = dict()
for animal in animals:
    exp_recrs[animal] = dict()
    for elec in elecs:
        exp_recrs[animal][elec] = dict()
        for musc in muscs:
            exp_recrs[animal][elec][musc] = dict()
            file_path = os.path.join(dir_import, animal, elec, '{}.txt'.format(musc))
            exp_recrs[animal][elec][musc]['recs'] = np.loadtxt(file_path)
        file_path = os.path.join(dir_import, animal, elec, 'Stim.txt')
        exp_recrs[animal][elec]['stim'] = np.loadtxt(file_path)
        exp_recrs[animal][elec]['amps'] = np.linspace(exp_recrs[animal][elec]['stim'][0],
                                                      exp_recrs[animal][elec]['stim'][1],
                                                      num=exp_recrs[animal][elec][muscs[0]]['recs'].size)


# Find threshold and saturation amplitudes of each muscle of each animal for each elec.
thresh_exp = dict()
sat_exp = dict()
for animal in animals:
    thresh_exp[animal] = dict()
    sat_exp[animal] = dict()
    for elec in elecs:
        thresh_exp[animal][elec] = dict()
        sat_exp[animal][elec] = dict()
        for musc in muscs:
            thresh_exp[animal][elec][musc] = dict()
            try:
                idx = np.flatnonzero(exp_recrs[animal][elec][musc]['recs'] >= xthresh)[0]
            except:
                idx = exp_recrs[animal][elec]['amps'].size - 1
            thresh_exp[animal][elec][musc]['amp'] = exp_recrs[animal][elec]['amps'][idx]
            thresh_exp[animal][elec][musc]['rec'] = exp_recrs[animal][elec][musc]['recs'][idx]
            sat_exp[animal][elec][musc] = dict()
            try:
                idx = np.flatnonzero(exp_recrs[animal][elec][musc]['recs'] >= xsat)[0]
                idx = min(idx + 2, exp_recrs[animal][elec]['amps'].size - 1)
            except:
                idx = exp_recrs[animal][elec]['amps'].size - 1
            sat_exp[animal][elec][musc]['amp'] = exp_recrs[animal][elec]['amps'][idx]
            sat_exp[animal][elec][musc]['rec'] = exp_recrs[animal][elec][musc]['recs'][idx]


# Load simulation results.
if mode == 1:
    dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
    dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_Ias_MNs_8muscs_5segs')
    dir_res = os.path.join(dir_smcd, 'results', 'smcd_Ias_MNs_8muscs_5segs')
    smc = SMC(dir_data=dir_data, dir_res=dir_res)
    entity = 'MN'
    amps = -25.0 * np.linspace(1, 80, num=80)
    muscs_gidxs = [smc.get_gidxs(entity, musc=musc) for musc in muscs]
    muscs_stim_types = [['stat_syn_from_Ia_gmax=9.625'  , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=5.0'    , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=3.375'  , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=7.625'  , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=10.5'   , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=5.75'   , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=15.375' , 'stat_syn_from_Ia_gmax=7.625'] ,
                        ['stat_syn_from_Ia_gmax=28.5'   , 'stat_syn_from_Ia_gmax=7.625']]

    # Format simulated recruitments.
    print('Loading records ...')
    simu_recrs = dict()
    for musc, gidxs, stim_types in zip(muscs, muscs_gidxs, muscs_stim_types):
        for gidx in gidxs:
            print('gidx #{:d} ...'.format(gidx))
            simu_recrs[gidx] = dict()
            record = smc.load_records(entity, gidx=gidx)
            for elec in elecs:
                simu_recrs[gidx][elec] = dict()
                for i, amp in enumerate(amps):
                    record1 = record.loc[(record.combi == elec) & (record.amp == amp) & (record.stim_type == stim_types[0])]
                    record2 = record.loc[(record.combi == elec) & (record.amp == amp) & (record.stim_type == stim_types[1])]
                    simu_recrs[gidx][elec][amp] = dict()
                    simu_recrs[gidx][elec][amp][0] = 1.0 if record1['recr'].any() else 0.0
                    simu_recrs[gidx][elec][amp][1] = 1.0 if record2['recr'].any() else 0.0

    if save:
        res = dict()
        res['muscs_gidxs'] = muscs_gidxs
        res['amps'] = amps
        res['simu_recrs'] = simu_recrs
        pickle.dump(res, open(os.path.join(dir_export, '{}.pck'.format(export_file_path_base)), 'wb'))

else:
    res = pickle.load(open(os.path.join(dir_export, '{}.pck'.format(export_file_path_base)), 'rb'))
    muscs_gidxs = res['muscs_gidxs']
    amps = res['amps']
    simu_recrs = res['simu_recrs']

# Find threshold and saturation amplitudes of each motor pool for each elec under each hypothesis.
thresh_h1 = dict()
thresh_h2 = dict()
sat_h1 = dict()
sat_h2 = dict()
for elec in elecs:
    thresh_h1[elec] = dict()
    thresh_h2[elec] = dict()
    sat_h1[elec] = dict()
    sat_h2[elec] = dict()
    for musc, gidxs in zip(muscs, muscs_gidxs):
        rec_curve_1 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][0] for gidx in gidxs]) for amp in amps])
        rec_curve_2 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][1] for gidx in gidxs]) for amp in amps])
        thresh_h1[elec][musc] = dict()
        idx = np.flatnonzero(rec_curve_1 >= xthresh)[0]
        thresh_h1[elec][musc]['amp'] = amps[idx]
        thresh_h1[elec][musc]['rec'] = rec_curve_1[idx]
        thresh_h2[elec][musc] = dict()
        idx = np.flatnonzero(rec_curve_2 >= xthresh)[0]
        thresh_h2[elec][musc]['amp'] = amps[idx]
        thresh_h2[elec][musc]['rec'] = rec_curve_2[idx]
        sat_h1[elec][musc] = dict()
        try:
            idx = np.flatnonzero(rec_curve_1 >= xsat)[0]
            idx = min(idx + 2, amps.size - 1)
        except:
            idx = amps.size - 1
        sat_h1[elec][musc]['amp'] = amps[idx]
        sat_h1[elec][musc]['rec'] = rec_curve_1[idx]
        sat_h2[elec][musc] = dict()
        try:
            idx = np.flatnonzero(rec_curve_2 >= xsat)[0]
            idx = min(idx + 2, amps.size - 1)
        except:
            idx = amps.size - 1
        sat_h2[elec][musc]['amp'] = amps[idx]
        sat_h2[elec][musc]['rec'] = rec_curve_2[idx]


# Compute cumulative root mean square error between simulated and experimental rec curves.
CRMSEs_exp_vs_h1 = np.zeros(len(animals))
CRMSEs_exp_vs_h2 = np.zeros(len(animals))

for n, animal in enumerate(animals):
    for elec in elecs:

        amp_thresh_exp = min(thresh_exp[animal][elec][musc]['amp'] for musc in muscs)
        amp_sat_exp = min(sat_exp[animal][elec][musc]['amp'] for musc in muscs)
        amps_exp_idxs = np.where((amp_thresh_exp <= exp_recrs[animal][elec]['amps']) &
                                 (amp_sat_exp >= exp_recrs[animal][elec]['amps']))
        amps_exp = exp_recrs[animal][elec]['amps'][amps_exp_idxs]

        amp_thresh_h1 = max(thresh_h1[elec][musc]['amp'] for musc in muscs)
        amp_sat_h1 = max(sat_h1[elec][musc]['amp'] for musc in muscs)
        amps_h1 = [amp for amp in amps if amp_sat_h1 <= amp <= amp_thresh_h1]

        amp_thresh_h2 = max(thresh_h2[elec][musc]['amp'] for musc in muscs)
        amp_sat_h2 = max(sat_h2[elec][musc]['amp'] for musc in muscs)
        amps_h2 = [amp for amp in amps if amp_sat_h2 <= amp <= amp_thresh_h2]

        for musc, gidxs in zip(muscs, muscs_gidxs):
            rec_curve_exp = exp_recrs[animal][elec][musc]['recs'][amps_exp_idxs]
            rec_curve_h1 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][0] for gidx in gidxs]) for amp in amps_h1])
            rec_curve_h2 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][1] for gidx in gidxs]) for amp in amps_h2])

            ys, xs = maths.resample([rec_curve_exp, rec_curve_h1], xs=[np.abs(amps_exp), np.abs(amps_h1)])
            CRMSEs_exp_vs_h1[n] += cost_fun(ys[0] - ys[1])

            ys, xs = maths.resample([rec_curve_exp, rec_curve_h2], xs=[np.abs(amps_exp), np.abs(amps_h2)])
            CRMSEs_exp_vs_h2[n] += cost_fun(ys[0] - ys[1])

print('')
print('CRMSE between exp and h1: {:f} (+/- {:f})'.format(CRMSEs_exp_vs_h1.mean(), CRMSEs_exp_vs_h1.std()))
print('CRMSE between exp and h2: {:f} (+/- {:f})'.format(CRMSEs_exp_vs_h2.mean(), CRMSEs_exp_vs_h2.std()))

# Compute threshold ranking errors between simulated and experimental rec curves.
TREs_exp_vs_h1 = np.zeros(len(animals))
TREs_exp_vs_h2 = np.zeros(len(animals))

for n, animal in enumerate(animals):
    for elec in elecs:

        ranks_exp = np.ones(len(muscs), dtype=int)
        for i in range(len(muscs)):
            for j in range(i):
                if thresh_exp[animal][elec][muscs[i]]['amp'] < thresh_exp[animal][elec][muscs[j]]['amp']:
                    ranks_exp[j] += 1
                elif (thresh_exp[animal][elec][muscs[i]]['amp'] == thresh_exp[animal][elec][muscs[j]]['amp']) and\
                        (thresh_exp[animal][elec][muscs[i]]['rec'] > thresh_exp[animal][elec][muscs[j]]['rec']):
                    ranks_exp[j] += 1
                else:
                    ranks_exp[i] = max(ranks_exp[i], ranks_exp[j] + 1)

        ranks_h1 = np.ones(len(muscs), dtype=int)
        for i in range(len(muscs)):
            for j in range(i):
                if thresh_h1[elec][muscs[i]]['amp'] < thresh_h1[elec][muscs[j]]['amp']:
                    ranks_h1[j] += 1
                elif thresh_h1[elec][muscs[i]]['amp'] == thresh_h1[elec][muscs[j]]['amp'] and \
                        thresh_h1[elec][muscs[i]]['rec'] > thresh_h1[elec][muscs[j]]['rec']:
                    ranks_h1[j] += 1
                else:
                    ranks_h1[i] = max(ranks_h1[i], ranks_h1[j] + 1)

        ranks_h2 = np.ones(len(muscs), dtype=int)
        for i in range(len(muscs)):
            for j in range(i):
                if thresh_h2[elec][muscs[i]]['amp'] < thresh_h2[elec][muscs[j]]['amp']:
                    ranks_h2[j] += 1
                elif thresh_h2[elec][muscs[i]]['amp'] == thresh_h2[elec][muscs[j]]['amp'] and \
                        thresh_h2[elec][muscs[i]]['rec'] > thresh_h2[elec][muscs[j]]['rec']:
                    ranks_h2[j] += 1
                else:
                    ranks_h2[i] = max(ranks_h2[i], ranks_h2[j] + 1)

        TREs_exp_vs_h1[n] += maths.get_inversion_number(
            maths.compose_permutations(ranks_h1, maths.invert_permutation(ranks_exp)))

        TREs_exp_vs_h2[n] += maths.get_inversion_number(
            maths.compose_permutations(ranks_h2, maths.invert_permutation(ranks_exp)))

print('')
print('TRE between exp and h1: {:f} (+/- {:f})'.format(TREs_exp_vs_h1.mean(), TREs_exp_vs_h1.std()))
print('TRE between exp and h2: {:f} (+/- {:f})'.format(TREs_exp_vs_h2.mean(), TREs_exp_vs_h2.std()))


# Compute discrepancy in saturation ranges between 1st series of simulated rec curves and experimental rec curves.
DREs_exp_vs_h1 = np.zeros(len(animals))
DREs_exp_vs_h2 = np.zeros(len(animals))

for n, animal in enumerate(animals):
    for elec in elecs:

        x1 = min([sat_exp[animal][elec][musc]['amp'] for musc in muscs])
        x2 = min([thresh_exp[animal][elec][musc]['amp'] for musc in muscs])
        dyn_range_exp = (x1 - x2) / x2

        x1 = max([sat_h1[elec][musc]['amp'] for musc in muscs])
        x2 = max([thresh_h1[elec][musc]['amp'] for musc in muscs])
        dyn_range_h1 = (x1 - x2) / x2

        x1 = max([sat_h2[elec][musc]['amp'] for musc in muscs])
        x2 = max([thresh_h2[elec][musc]['amp'] for musc in muscs])
        dyn_range_h2 = (x1 - x2) / x2

        DREs_exp_vs_h1[n] += dyn_range_h1 / dyn_range_exp + dyn_range_exp / dyn_range_h1 - 2.0

        DREs_exp_vs_h2[n] += dyn_range_h2 / dyn_range_exp + dyn_range_exp / dyn_range_h2 - 2.0

print('')
print('DRE between exp and h1: {:f} (+/- {:f})'.format(DREs_exp_vs_h1.mean(), DREs_exp_vs_h1.std()))
print('DRE between exp and h2: {:f} (+/- {:f})'.format(DREs_exp_vs_h2.mean(), DREs_exp_vs_h2.std()))
print('')


# Compute statistics of the CRMSE, TRE and DRE between simulated rec curves.
if mode in {1, 2}:
    CRMSEs_h1_vs_h2 = np.zeros(pop_size)
    TREs_h1_vs_h2 = np.zeros(pop_size)
    DREs_h1_vs_h2 = np.zeros(pop_size)

    for n in range(pop_size):

        print('pop #{:d}'.format(n + 1))

        # Form populations.
        pop_gidxs = list()
        for gidxs in muscs_gidxs:
            bstrap = Bootstrap(pop_size=1)
            bstrap.set_data_sequence(gidxs)
            bstrap.gen_pop()
            pop_gidxs.append(bstrap.pop[:, 0])

        # Compute threshold and saturation amplitudes of populations.
        pop_thresh_h1 = dict()
        pop_thresh_h2 = dict()
        pop_sat_h1 = dict()
        pop_sat_h2 = dict()
        for elec in elecs:
            pop_thresh_h1[elec] = dict()
            pop_thresh_h2[elec] = dict()
            pop_sat_h1[elec] = dict()
            pop_sat_h2[elec] = dict()
            for musc, gidxs in zip(muscs, pop_gidxs):
                rec_curve_1 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][0] for gidx in gidxs]) for amp in amps])
                rec_curve_2 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][1] for gidx in gidxs]) for amp in amps])
                pop_thresh_h1[elec][musc] = dict()
                try:
                    idx = np.flatnonzero(rec_curve_1 >= xthresh)[0]
                except:
                    idx = amps.size - 1
                pop_thresh_h1[elec][musc]['amp'] = amps[idx]
                pop_thresh_h1[elec][musc]['rec'] = rec_curve_1[idx]
                pop_thresh_h2[elec][musc] = dict()
                try:
                    idx = np.flatnonzero(rec_curve_2 >= xthresh)[0]
                except:
                    idx = amps.size - 1
                pop_thresh_h2[elec][musc]['amp'] = amps[idx]
                pop_thresh_h2[elec][musc]['rec'] = rec_curve_2[idx]
                pop_sat_h1[elec][musc] = dict()
                try:
                    idx = np.flatnonzero(rec_curve_1 >= xsat)[0]
                    idx = min(idx + 2, amps.size - 1)
                except:
                    idx = amps.size - 1
                pop_sat_h1[elec][musc]['amp'] = amps[idx]
                pop_sat_h1[elec][musc]['rec'] = rec_curve_1[idx]
                pop_sat_h2[elec][musc] = dict()
                try:
                    idx = np.flatnonzero(rec_curve_2 >= xsat)[0]
                    idx = min(idx + 2, amps.size - 1)
                except:
                    idx = amps.size - 1
                pop_sat_h2[elec][musc]['amp'] = amps[idx]
                pop_sat_h2[elec][musc]['rec'] = rec_curve_2[idx]

        # Compute CRMSE.
        for elec in elecs:

            amp_thresh_h1 = max(pop_thresh_h1[elec][musc]['amp'] for musc in muscs)
            amp_sat_h1 = max(pop_sat_h1[elec][musc]['amp'] for musc in muscs)
            amps_h1 = [amp for amp in amps if amp_sat_h1 <= amp <= amp_thresh_h1]

            amp_thresh_h2 = max(pop_thresh_h2[elec][musc]['amp'] for musc in muscs)
            amp_sat_h2 = max(pop_sat_h2[elec][musc]['amp'] for musc in muscs)
            amps_h2 = [amp for amp in amps if amp_sat_h2 <= amp <= amp_thresh_h2]

            for musc, gidxs in zip(muscs, pop_gidxs):
                rec_curve_1 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][0] for gidx in gidxs]) for amp in amps_h1])
                rec_curve_2 = np.asarray([np.mean([simu_recrs[gidx][elec][amp][1] for gidx in gidxs]) for amp in amps_h2])
                ys, xs = maths.resample([rec_curve_1, rec_curve_2], xs=[np.abs(amps_h1), np.abs(amps_h2)])
                CRMSEs_h1_vs_h2[n] += cost_fun(ys[0] - ys[1])

        # Compute TRE.
        for elec in elecs:

            ranks_h1 = np.ones(len(muscs), dtype=int)
            for i in range(len(muscs)):
                for j in range(i):
                    if pop_thresh_h1[elec][muscs[i]]['amp'] < pop_thresh_h1[elec][muscs[j]]['amp']:
                        ranks_h1[j] += 1
                    elif pop_thresh_h1[elec][muscs[i]]['amp'] == pop_thresh_h1[elec][muscs[j]]['amp'] and \
                            pop_thresh_h1[elec][muscs[i]]['rec'] > pop_thresh_h1[elec][muscs[j]]['rec']:
                        ranks_h1[j] += 1
                    else:
                        ranks_h1[i] = max(ranks_h1[i], ranks_h1[j] + 1)

            ranks_h2 = np.ones(len(muscs), dtype=int)
            for i in range(len(muscs)):
                for j in range(i):
                    if pop_thresh_h2[elec][muscs[i]]['amp'] < pop_thresh_h2[elec][muscs[j]]['amp']:
                        ranks_h2[j] += 1
                    elif pop_thresh_h2[elec][muscs[i]]['amp'] == pop_thresh_h2[elec][muscs[j]]['amp'] and \
                            pop_thresh_h2[elec][muscs[i]]['rec'] > pop_thresh_h2[elec][muscs[j]]['rec']:
                        ranks_h2[j] += 1
                    else:
                        ranks_h2[i] = max(ranks_h2[i], ranks_h2[j] + 1)

            TREs_h1_vs_h2[n] += maths.get_inversion_number(
                maths.compose_permutations(ranks_h1, maths.invert_permutation(ranks_h2)))

        # Compute DRE.
        for elec in elecs:

            x1 = max([pop_sat_h1[elec][musc]['amp'] for musc in muscs])
            x2 = max([pop_thresh_h1[elec][musc]['amp'] for musc in muscs])
            dyn_range_h1 = (x1 - x2) / x2

            x1 = max([pop_sat_h2[elec][musc]['amp'] for musc in muscs])
            x2 = max([pop_thresh_h2[elec][musc]['amp'] for musc in muscs])
            dyn_range_h2 = (x1 - x2) / x2

            DREs_h1_vs_h2[n] += dyn_range_h1 / dyn_range_h2 + dyn_range_h2 / dyn_range_h1 - 2.0

    if save:
        res['CRMSEs_h1_vs_h2'] = CRMSEs_h1_vs_h2
        res['TREs_h1_vs_h2'] = TREs_h1_vs_h2
        res['DREs_h1_vs_h2'] = DREs_h1_vs_h2
        pickle.dump(res, open(os.path.join(dir_export, '{}.pck'.format(export_file_path_base)), 'wb'))

else:
    CRMSEs_h1_vs_h2 = res['CRMSEs_h1_vs_h2']
    TREs_h1_vs_h2 = res['TREs_h1_vs_h2']
    DREs_h1_vs_h2 = res['DREs_h1_vs_h2']


print('')
print('CRMSE between h1 and h2: {:f} (+/- {:f})'.format(CRMSEs_h1_vs_h2.mean(), CRMSEs_h1_vs_h2.std()))
print('TRE between h1 and h2: {:f} (+/- {:f})'.format(TREs_h1_vs_h2.mean(), TREs_h1_vs_h2.std()))
print('DRE between h1 and h2: {:f} (+/- {:f})'.format(DREs_h1_vs_h2.mean(), DREs_h1_vs_h2.std()))
print('')


# Plotting section.

# Plotting parameters.
pp = dict()
pp['with_xtick'] = False
pp['with_xaxis'] = False

# Create figure.
plotter = Plotter()
fig, axs = plotter.build_figure(3, ncol=3)

# Plot CRMSEs.
pp['ax_title'] = 'Cumulative RMSE'
pp['xlim'] = [0.5, 2.5]
pp['ylim'] = [0.0, 10.0]
pp['colors'] = ['gray'] * 2
pp['alpha_fills'] = [1.0] * 2
pp['sizes'] = [169.0, 324.0]
pp['line_widths'] = [0.5] * 2
xs = np.ones_like(CRMSEs_exp_vs_h1)
plotter.plot_dots(axs[0], xs, CRMSEs_exp_vs_h1, 1.0, np.median(CRMSEs_exp_vs_h1), **pp)
pp['alpha_fills'] = [0.5] * 2
xs = 2.0 * np.ones_like(CRMSEs_exp_vs_h2)
plotter.plot_dots(axs[0], xs, CRMSEs_exp_vs_h2, 2.0, np.median(CRMSEs_exp_vs_h2), **pp)
pp['colors'] = ['red']
pp['alpha_fills'] = [0.5]
xs = np.asarray([0.5, 2.5])
ys = np.asarray([CRMSEs_h1_vs_h2.mean()] * 2)
yerrs = np.asarray([CRMSEs_h1_vs_h2.std()] * 2)
plotter.plot_bounded_lines(axs[0], xs, ys, yerrs, **pp)

# Plot TREs.
pp['ax_title'] = 'Threshold Ranking Error'
pp['ylim'] = [0.0, 70.0]
pp['colors'] = ['gray'] * 2
pp['alpha_fills'] = [1.0] * 2
xs = np.ones_like(TREs_exp_vs_h1)
plotter.plot_dots(axs[1], xs, TREs_exp_vs_h1, 1.0, np.median(TREs_exp_vs_h1), **pp)
pp['alpha_fills'] = [0.5] * 2
xs = 2.0 * np.ones_like(TREs_exp_vs_h2)
plotter.plot_dots(axs[1], xs, TREs_exp_vs_h2, 2.0, np.median(TREs_exp_vs_h2), **pp)
pp['colors'] = ['red']
pp['alpha_fills'] = [0.5]
xs = np.asarray([0.5, 2.5])
ys = np.asarray([TREs_h1_vs_h2.mean()] * 2)
yerrs = np.asarray([TREs_h1_vs_h2.std()] * 2)
plotter.plot_bounded_lines(axs[1], xs, ys, yerrs, **pp)

# Plot DREs.
pp['ax_title'] = 'Dynamic Range Error'
pp['ylim'] = [0.0, 15.0]
pp['colors'] = ['gray'] * 2
pp['alpha_fills'] = [1.0] * 2
xs = np.ones_like(DREs_exp_vs_h1)
plotter.plot_dots(axs[2], xs, DREs_exp_vs_h1, 1.0, np.median(DREs_exp_vs_h1), **pp)
pp['alpha_fills'] = [0.5] * 2
xs = 2.0 * np.ones_like(DREs_exp_vs_h2)
plotter.plot_dots(axs[2], xs, DREs_exp_vs_h2, 2.0, np.median(DREs_exp_vs_h2), **pp)
pp['colors'] = ['red']
pp['alpha_fills'] = [0.5]
xs = np.asarray([0.5, 2.5])
ys = np.asarray([DREs_h1_vs_h2.mean()] * 2)
yerrs = np.asarray([DREs_h1_vs_h2.std()] * 2)
plotter.plot_bounded_lines(axs[2], xs, ys, yerrs, **pp)

# Legends.
fig.legend(axs[0].collections[0:1] + axs[0].collections[2:3] + axs[0].lines, ['H1 vs Exp.', 'H2 vs Exp.', 'H1 vs H2'],
    ncol=3, loc='lower center', bbox_to_anchor=(0.5, 0.0))

plotter.tight_layout(pad=4)

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(export_file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(export_file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
