# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_figs_2b_2f_1.py
#
# Created on : 17 July 2018
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the scripts 'CST.py', 'DCcB.py',
#   'Ia_1.py', 'II.py', 'MN_Eext.py' and 'ST.py'.
#
#############################################################################


import os
import pickle
import numpy as np

from smcees.neural_entities.SMC import SMC
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils.Plotter import Plotter
from smcees.utils import maths


# Choose and load sensorimotor circuit dataset.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose electrode.
# Choose stimulation amplitudes.
entities = ['Ia', 'II', 'DCcB', 'CST', 'ST', 'MN']
seg = 'C6'
combis = ['elec10', 'elec11']
amps = - 25.0 * np.linspace(1, 60, num=60)
pop_size = 10000

# Basename for figure/text/binary files.
folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name_base = 'figs_2b_2f_1'
file_path_base = os.path.join(folder_path, file_name_base)

# Choose mode:
# 1. compute new recruitment curves and build plots (new results can be saved or not)
# 2. import previous recruitment curves and build plots
mode = 1
save = True

# If mode=1, compute new recruitment curves.
if mode == 1:

    Ia_gidxs = smc.get_Ia_gidxs(seg=seg)
    II_gidxs = smc.get_II_gidxs(seg=seg)
    DCcB_gidxs = smc.get_DCcB_gidxs(seg=seg)
    CST_gidxs = smc.get_CST_gidxs(seg=seg)
    ST_gidxs = smc.get_ST_gidxs(seg=seg)
    MN_gidxs = smc.get_MN_gidxs(seg=seg)

    # Load Ias records and build dictionary to cache the retrieved recruitment values.
    Ias_records = dict()
    Ias_recrs = dict()
    for gidx in Ia_gidxs:
        Ias_records[gidx] = smc.load_records('Ia', gidx=gidx)
        Ias_recrs[gidx] = dict()
        for combi in combis:
            Ias_recrs[gidx][combi] = dict()

    # Load IIs records and build dictionary to cache the retrieved recruitment values.
    IIs_records = dict()
    IIs_recrs = dict()
    for gidx in II_gidxs:
        IIs_records[gidx] = smc.load_records('II', gidx=gidx)
        IIs_recrs[gidx] = dict()
        for combi in combis:
            IIs_recrs[gidx][combi] = dict()

    # Load DCcBs records and build dictionary to cache the retrieved recruitment values.
    DCcBs_records = dict()
    DCcBs_recrs = dict()
    for gidx in DCcB_gidxs:
        DCcBs_records[gidx] = smc.load_records('DCcB', gidx=gidx)
        DCcBs_recrs[gidx] = dict()
        for combi in combis:
            DCcBs_recrs[gidx][combi] = dict()

    # Load CSTs records and build dictionary to cache the retrieved recruitment values.
    CSTs_records = dict()
    CSTs_recrs = dict()
    for gidx in CST_gidxs:
        CSTs_records[gidx] = smc.load_records('CST', gidx=gidx)
        CSTs_recrs[gidx] = dict()
        for combi in combis:
            CSTs_recrs[gidx][combi] = dict()

    # Load STs records and build dictionary to cache the retrieved recruitment values.
    STs_records = dict()
    STs_recrs = dict()
    for gidx in ST_gidxs:
        STs_records[gidx] = smc.load_records('ST', gidx=gidx)
        STs_recrs[gidx] = dict()
        for combi in combis:
            STs_recrs[gidx][combi] = dict()

    # Load MNs records and build dictionary to cache the retrieved recruitment values.
    MNs_records = dict()
    MNs_recrs = dict()
    for gidx in MN_gidxs:
        MNs_records[gidx] = smc.load_records('MN', gidx=gidx)
        MNs_recrs[gidx] = dict()
        for combi in combis:
            MNs_recrs[gidx][combi] = dict()

    # Compute recruitment curves using a Bootstrapping approach.

    Ia_recs = dict()
    Ia_stds = dict()
    II_recs = dict()
    II_stds = dict()
    DCcB_recs = dict()
    DCcB_stds = dict()
    CST_recs = dict()
    CST_stds = dict()
    ST_recs = dict()
    ST_stds = dict()
    MN_recs = dict()
    MN_stds = dict()

    for combi in combis:

        Ia_rec = np.zeros(amps.size)
        Ia_std = np.zeros(amps.size)
        II_rec = np.zeros(amps.size)
        II_std = np.zeros(amps.size)
        DCcB_rec = np.zeros(amps.size)
        DCcB_std = np.zeros(amps.size)
        CST_rec = np.zeros(amps.size)
        CST_std = np.zeros(amps.size)
        ST_rec = np.zeros(amps.size)
        ST_std = np.zeros(amps.size)
        MN_rec = np.zeros(amps.size)
        MN_std = np.zeros(amps.size)

        Ia_bstrap = Bootstrap(pop_size=pop_size)
        Ia_bstrap.set_data_sequence(Ia_gidxs)
        Ia_bstrap.gen_pop()

        II_bstrap = Bootstrap(pop_size=pop_size)
        II_bstrap.set_data_sequence(II_gidxs)
        II_bstrap.gen_pop()

        DCcB_bstrap = Bootstrap(pop_size=pop_size)
        DCcB_bstrap.set_data_sequence(DCcB_gidxs)
        DCcB_bstrap.gen_pop()

        CST_bstrap = Bootstrap(pop_size=pop_size)
        CST_bstrap.set_data_sequence(CST_gidxs)
        CST_bstrap.gen_pop()

        ST_bstrap = Bootstrap(pop_size=pop_size)
        ST_bstrap.set_data_sequence(CST_gidxs)
        ST_bstrap.gen_pop()

        MN_bstrap = Bootstrap(pop_size=pop_size)
        MN_bstrap.set_data_sequence(MN_gidxs)
        MN_bstrap.gen_pop()

        for i, amp in enumerate(amps):

            print('amp = {:f}'.format(amp))

            amp_Ias_recs = np.zeros((len(Ia_gidxs), pop_size))
            amp_Ias_stds = np.zeros((len(Ia_gidxs), pop_size))
            amp_IIs_recs = np.zeros((len(II_gidxs), pop_size))
            amp_IIs_stds = np.zeros((len(II_gidxs), pop_size))
            amp_DCcBs_recs = np.zeros((len(DCcB_gidxs), pop_size))
            amp_DCcBs_stds = np.zeros((len(DCcB_gidxs), pop_size))
            amp_CSTs_recs = np.zeros((len(CST_gidxs), pop_size))
            amp_CSTs_stds = np.zeros((len(CST_gidxs), pop_size))
            amp_STs_recs = np.zeros((len(CST_gidxs), pop_size))
            amp_STs_stds = np.zeros((len(CST_gidxs), pop_size))
            amp_MNs_recs = np.zeros((len(MN_gidxs), pop_size))
            amp_MNs_stds = np.zeros((len(MN_gidxs), pop_size))

            for j in range(pop_size):

                for k, gidx in enumerate(Ia_bstrap.pop[:, j]):
                    try:
                        amp_Ias_recs[k, j] = Ias_recrs[gidx][combi][amp]
                    except:
                        records = Ias_records[gidx].loc[(Ias_records[gidx].combi == combi) & (Ias_records[gidx].amp == amp)]
                        amp_Ias_recs[k, j] = 1.0 if records['recr'].any() else 0.0
                        Ias_recrs[gidx][combi][amp] = amp_Ias_recs[k, j]

                for k, gidx in enumerate(II_bstrap.pop[:, j]):
                    try:
                        amp_IIs_recs[k, j] = IIs_recrs[gidx][combi][amp]
                    except:
                        records = IIs_records[gidx].loc[(IIs_records[gidx].combi == combi) & (IIs_records[gidx].amp == amp)]
                        amp_IIs_recs[k, j] = 1.0 if records['recr'].any() else 0.0
                        IIs_recrs[gidx][combi][amp] = 1.0 if records['recr'].any() else 0.0

                for k, gidx in enumerate(DCcB_bstrap.pop[:, j]):
                    try:
                        amp_DCcBs_recs[k, j] = DCcBs_recrs[gidx][combi][amp]
                    except:
                        records = DCcBs_records[gidx].loc[(DCcBs_records[gidx].combi == combi) &
                                                          (DCcBs_records[gidx].amp == amp)]
                        amp_DCcBs_recs[k, j] = 1.0 if records['recr'].any() else 0.0
                        DCcBs_recrs[gidx][combi][amp] = 1.0 if records['recr'].any() else 0.0

                for k, gidx in enumerate(CST_bstrap.pop[:, j]):
                    try:
                        amp_CSTs_recs[k, j] = CSTs_recrs[gidx][combi][amp]
                    except:
                        records = CSTs_records[gidx].loc[(CSTs_records[gidx].combi == combi) &
                                                          (CSTs_records[gidx].amp == amp)]
                        amp_CSTs_recs[k, j] = 1.0 if records['recr'].any() else 0.0
                        CSTs_recrs[gidx][combi][amp] = 1.0 if records['recr'].any() else 0.0

                for k, gidx in enumerate(ST_bstrap.pop[:, j]):
                    try:
                        amp_STs_recs[k, j] = STs_recrs[gidx][combi][amp]
                    except:
                        records = STs_records[gidx].loc[(STs_records[gidx].combi == combi) &
                                                          (STs_records[gidx].amp == amp)]
                        amp_STs_recs[k, j] = 1.0 if records['recr'].any() else 0.0
                        STs_recrs[gidx][combi][amp] = 1.0 if records['recr'].any() else 0.0

                for k, gidx in enumerate(MN_bstrap.pop[:, j]):
                    try:
                        amp_MNs_recs[k, j] = MNs_recrs[gidx][combi][amp]
                    except:
                        records = MNs_records[gidx].loc[(MNs_records[gidx].combi == combi) &
                                                          (MNs_records[gidx].amp == amp)]
                        amp_MNs_recs[k, j] = 1.0 if records['recr'].any() else 0.0
                        MNs_recrs[gidx][combi][amp] = 1.0 if records['recr'].any() else 0.0

            amp_Ias_recs = np.mean(amp_Ias_recs, axis=0) * 100.0
            Ia_rec[i] = np.mean(amp_Ias_recs)
            Ia_std[i] = np.std(amp_Ias_recs)

            amp_IIs_recs = np.mean(amp_IIs_recs, axis=0) * 100.0
            II_rec[i] = np.mean(amp_IIs_recs)
            II_std[i] = np.std(amp_IIs_recs)

            amp_DCcBs_recs = np.mean(amp_DCcBs_recs, axis=0) * 100.0
            DCcB_rec[i] = np.mean(amp_DCcBs_recs)
            DCcB_std[i] = np.std(amp_DCcBs_recs)

            amp_CSTs_recs = np.mean(amp_CSTs_recs, axis=0) * 100.0
            CST_rec[i] = np.mean(amp_CSTs_recs)
            CST_std[i] = np.std(amp_CSTs_recs)

            amp_STs_recs = np.mean(amp_STs_recs, axis=0) * 100.0
            ST_rec[i] = np.mean(amp_STs_recs)
            ST_std[i] = np.std(amp_STs_recs)

            amp_MNs_recs = np.mean(amp_MNs_recs, axis=0) * 100.0
            MN_rec[i] = np.mean(amp_MNs_recs)
            MN_std[i] = np.std(amp_MNs_recs)

        Ia_recs[combi] = Ia_rec
        Ia_stds[combi] = Ia_std
        II_recs[combi] = II_rec
        II_stds[combi] = II_std
        DCcB_recs[combi] = DCcB_rec
        DCcB_stds[combi] = DCcB_std
        CST_recs[combi] = CST_rec
        CST_stds[combi] = CST_std
        ST_recs[combi] = ST_rec
        ST_stds[combi] = ST_std
        MN_recs[combi] = MN_rec
        MN_stds[combi] = MN_std

    # Find activation amplitude thresholds.
    ratio = 10.0
    thresholds = []
    for rec in list(Ia_recs.values()) + list(II_recs.values()) + list(DCcB_recs.values()) + list(CST_recs.values()) + list(ST_recs.values()) + list(MN_recs.values()):
        try:
            iIxxh = np.flatnonzero(rec >= ratio)[0]
            iIxxl = np.flatnonzero(rec <= ratio)[-1]
            Ixxh = amps[iIxxh]
            Ixxl = amps[iIxxl]
            Axxh = rec[iIxxh]
            Axxl = rec[iIxxl]
            if Ixxh == Ixxl:
                Ixx = Ixxl
            elif Axxh == Axxl:
                Ixx = Ixxl
            else:
                Ixx = Ixxl + (Ixxh - Ixxl) / (Axxh - Axxl) * (ratio - Axxl)
        except:
            Ixx = amps[-1]
        thresholds.append(Ixx)
    Ixx0 = max(thresholds)  # amplitudes are negative until here

    # Normalize amplitudes.
    amps = amps / Ixx0

    # Save results.
    if save:

        # Write data to textfile.
        text = '****************************************'
        text += 'Amplitudes\n'
        text += ' '.join(['{:.1f}'.format(amp) for amp in amps])
        text += '\n'
        text += '****************************************\n'
        text += 'Recruitment Curves\n'
        text += '\n'
        for combi in combis:
            text += 'combi: {}\n'.format(combi)
            text += '\n'
            text += 'Normalized Mean Ia recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in Ia_recs[combi]])
            text += '\n'
            text += 'Normalized STDS Ia recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in Ia_stds[combi]])
            text += '\n'
            text += 'Normalized Mean II recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in II_recs[combi]])
            text += '\n'
            text += 'Normalized STDS II recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in II_stds[combi]])
            text += '\n'
            text += 'Normalized Mean DCcB recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in DCcB_recs[combi]])
            text += '\n'
            text += 'Normalized STDS DCcB recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in DCcB_stds[combi]])
            text += '\n'
            text += 'Normalized Mean CST recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in CST_recs[combi]])
            text += '\n'
            text += 'Normalized STDS CST recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in CST_stds[combi]])
            text += '\n'
            text += 'Normalized Mean ST recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in ST_recs[combi]])
            text += '\n'
            text += 'Normalized STDS ST recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in ST_stds[combi]])
            text += '\n'
            text += 'Normalized Mean MN recruitments\n'
            text += ' '.join(['{:.4f}'.format(rec) for rec in MN_recs[combi]])
            text += '\n'
            text += 'Normalized STDS MN recruitments\n'
            text += ' '.join(['{:.4f}'.format(std) for std in MN_stds[combi]])
            text += '\n'
            with open('{}.txt'.format(file_path_base), 'w') as fileID:
                fileID.write(text)

        # Save binary results.
        res = dict(Ia_recs=Ia_recs, Ia_stds=Ia_stds, II_recs=II_recs, II_stds=II_stds, DCcB_recs=DCcB_recs,
                   DCcB_stds=DCcB_stds, CST_recs=CST_recs, CST_stds=CST_stds, ST_recs=ST_recs, ST_stds=ST_stds,
                   MN_recs=MN_recs, MN_stds=MN_stds, amps=amps)
        pickle.dump(res, open('{}.pck'.format(file_path_base), 'wb'))


# Otherwise, import previous results.
else:
    res = pickle.load(open('{}.pck'.format(file_path_base), 'rb'))
    Ia_recs = res['Ia_recs']
    Ia_stds = res['Ia_stds']
    II_recs = res['II_recs']
    II_stds = res['II_stds']
    DCcB_recs = res['DCcB_recs']
    DCcB_stds = res['DCcB_stds']
    CST_recs = res['CST_recs']
    CST_stds = res['CST_stds']
    ST_recs = res['ST_recs']
    ST_stds = res['ST_stds']
    MN_recs = res['MN_recs']
    MN_stds = res['MN_stds']
    amps = res['amps']

# Smooth curves.
for combi in combis:
    Ia_recs[combi] = maths.running_mean(Ia_recs[combi])
    Ia_stds[combi] = maths.running_mean(Ia_stds[combi])
    II_recs[combi] = maths.running_mean(II_recs[combi])
    II_stds[combi] = maths.running_mean(II_stds[combi])
    DCcB_recs[combi] = maths.running_mean(DCcB_recs[combi])
    DCcB_stds[combi] = maths.running_mean(DCcB_stds[combi])
    CST_recs[combi] = maths.running_mean(CST_recs[combi])
    CST_stds[combi] = maths.running_mean(CST_stds[combi])
    ST_recs[combi] = maths.running_mean(ST_recs[combi])
    ST_stds[combi] = maths.running_mean(ST_stds[combi])
    MN_recs[combi] = maths.running_mean(MN_recs[combi])
    MN_stds[combi] = maths.running_mean(MN_stds[combi])


# Plotting section.

# Define global plotting parameters.
pp = dict()
pp['ylabel'] = 'recruited entities (%)'
pp['xlabel'] = 'normalized amplitude'
pp['colors'] = Plotter.get_colors('viridis', 6, lthresh=0.0, hthresh=1.0)
pp['ylim'] = [0.0, 100.0]
pp['xlim'] = [0.0, amps[-1]]
pp['tight_layout'] = True

# Create figure.
plotter = Plotter(**pp)
fig, axs = plotter.build_figure(2, ncol=2)

# Plot recruitment curves.
for i, combi in enumerate(combis):
    pp['ax_title'] = combi
    plotter.plot_bounded_lines(axs[i],
                               amps, Ia_recs[combi], Ia_stds[combi],
                               amps, II_recs[combi], II_stds[combi],
                               amps, DCcB_recs[combi], DCcB_stds[combi],
                               amps, CST_recs[combi], CST_stds[combi],
                               amps, ST_recs[combi], ST_stds[combi],
                               amps, MN_recs[combi], MN_stds[combi], **pp)

# Legends.
fig.legend(axs[0].lines, entities, ncol=6, loc='lower center', bbox_to_anchor=(0.5, 0.0))

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
