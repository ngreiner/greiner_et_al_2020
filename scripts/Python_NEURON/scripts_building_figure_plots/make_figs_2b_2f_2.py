# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_figs_2b_2f_2.py
#
# Created on : 26 September 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script should be executed after the scripts 'CST.py', 'DCcB.py',
#   'Ia_1.py', 'II.py', 'MN_Eext.py' and 'ST.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.utils.Plotter import Plotter
from smcees.utils.Bootstrap import Bootstrap


# Choose and load sensorimotor circuit dataset.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose entities.
# Choose combis.
# Choose stimulation amplitudes.
entities = ['Ia', 'II', 'DCcB', 'CST', 'ST', 'MN']
seg = 'C6'
combis = ['elec10', 'elec11']
amps2000 = - 25.0 * np.linspace(1, 80, num=80)
amps8000 = - 25.0 * np.linspace(1, 320, num=320)
entities_amps = [amps2000, amps2000, amps2000, amps2000, amps2000, amps8000]
pop_size = 10000

# Basename for figure/text/binary files.
folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name_base = 'figs_2b_2f_2'
file_path_base = os.path.join(folder_path, file_name_base)

# Choose mode:
# 1. compute new threshold and saturation levels and build plots (new results can be saved or not)
# 2. import previous threshold and saturation levels and build plots
mode = 1
save = True

# If mode=1, compute new threshold and saturation levels.
if mode == 1:

    # Prepare dictionaries of entity threshold and saturation amplitudes.
    I10_means = dict()
    I10_stds = dict()
    I90_means = dict()
    I90_stds = dict()
    for combi in combis:
        I10_means[combi] = list()
        I10_stds[combi] = list()
        I90_means[combi] = list()
        I90_stds[combi] = list()

    # Prepare lists for storing the entities' recruitment levels' amplitude distributions.
    entities_combis_recs_amps = list()

    # Compute threshold and saturation using a Bootstrapping approach.
    for entity, amps in zip(entities, entities_amps):

        print('*' * 50)
        print('entity = {:}\n'.format(entity))

        # Retrieve entity gidxs.
        gidxs = smc.get_gidxs(entity, seg=seg)

        # Load entity instances' records and prepare dictionaries for caching retrieved recruitments.
        records = dict()
        recrs = dict()
        for gidx in gidxs:
            records[gidx] = smc.load_obj(entity + 'records', entity, gidx=gidx)
            recrs[gidx] = dict()
            for combi in combis:
                recrs[gidx][combi] = dict()

        # Build Bootstrap object to perform statistical analyses.
        bstrap = Bootstrap(pop_size=pop_size)
        bstrap.set_data_sequence(gidxs)
        bstrap.gen_pop()

        # Dictionary to store the entity's recruitment levels' amplitude distributions for the different combis.
        combis_recs_amps = dict()

        for combi in combis:

            print('-' * 50)
            print('combi = {:}\n'.format(combi))

            recs_amps = dict()

            for i, amp in enumerate(amps):

                print('amp = {:f}'.format(amp))

                amp_recs = np.zeros((len(gidxs), pop_size))

                for j in range(pop_size):

                    # print 'Bootstrap sample: {:d}'.format(j)

                    for k, gidx in enumerate(bstrap.pop[:, j]):
                        try:
                            amp_recs[k, j] = recrs[gidx][combi][amp]
                        except:
                            record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)]
                            amp_recs[k, j] = 1.0 if record['recr'].any() else 0.0
                            recrs[gidx][combi][amp] = amp_recs[k, j]

                tmp = np.mean(amp_recs, axis=0)

                for rec in tmp:
                    try:
                        recs_amps[rec].append(amp)
                    except:
                        recs_amps[rec] = [amp]

                # print recs_amps

            combis_recs_amps[combi] = recs_amps

        entities_combis_recs_amps.append(combis_recs_amps)

    # Extract statistics of threshold and saturation amplitudes.
    for combi in combis:
        print('-' * 50)
        print('combi = {:}\n'.format(combi))

        for combis_recs_amps, entity in zip(entities_combis_recs_amps, entities):
            print('*' * 50)
            print('entity = {:}\n'.format(entity))
            print(combis_recs_amps[combi].keys())

            rec_amps = np.asarray(combis_recs_amps[combi][0.1])
            I10_means[combi].append(np.mean(rec_amps))
            I10_stds[combi].append(np.std(rec_amps))
            rec_amps = np.asarray(combis_recs_amps[combi][0.9])
            I90_means[combi].append(np.mean(rec_amps))
            I90_stds[combi].append(np.std(rec_amps))

    # Rectify (normalize) amplitudes.
    I0 = max(I10_means[combis[0]] + I10_means[combis[1]])
    for combi in combis:
        I10_means[combi] = np.asarray(I10_means[combi]) / I0
        I10_stds[combi] = np.asarray(I10_stds[combi]) / I0
        I90_means[combi] = np.asarray(I90_means[combi]) / I0
        I90_stds[combi] = np.asarray(I90_stds[combi]) / I0

    # Save results.
    if save:

        # Write data to textfile.
        text = ''
        for combi in combis:
            text += '****************************************\n'
            text += 'combi: {}\n\n'.format(combi)
            text += 'Mean threshold Ia: {:.4f}\n'.format(I10_means[combi][0])
            text += 'STD threshold Ia: {:.4f}\n'.format(I10_stds[combi][0])
            text += 'Mean threshold II: {:.4f}\n'.format(I10_means[combi][1])
            text += 'STD threshold II: {:.4f}\n'.format(I10_stds[combi][1])
            text += 'Mean threshold DCcB: {:.4f}\n'.format(I10_means[combi][2])
            text += 'STD threshold DCcB: {:.4f}\n'.format(I10_stds[combi][2])
            text += 'Mean threshold CST: {:.4f}\n'.format(I10_means[combi][3])
            text += 'STD threshold CST: {:.4f}\n'.format(I10_stds[combi][3])
            text += 'Mean threshold MN: {:.4f}\n'.format(I10_means[combi][4])
            text += 'STD threshold MN: {:.4f}\n'.format(I10_stds[combi][4])
            text += '\n'
            text += 'Mean saturation Ia: {:.4f}\n'.format(I10_means[combi][0])
            text += 'STD saturation Ia: {:.4f}\n'.format(I10_stds[combi][0])
            text += 'Mean saturation II: {:.4f}\n'.format(I10_means[combi][1])
            text += 'STD saturation II: {:.4f}\n'.format(I10_stds[combi][1])
            text += 'Mean saturation DCcB: {:.4f}\n'.format(I10_means[combi][2])
            text += 'STD saturation DCcB: {:.4f}\n'.format(I10_stds[combi][2])
            text += 'Mean saturation CST: {:.4f}\n'.format(I10_means[combi][3])
            text += 'STD saturation CST: {:.4f}\n'.format(I10_stds[combi][3])
            text += 'Mean saturation MN: {:.4f}\n'.format(I10_means[combi][4])
            text += 'STD saturation MN: {:.4f}\n'.format(I10_stds[combi][4])
            text += '\n\n'
            with open('{}.txt'.format(file_path_base), 'w') as fileID:
                fileID.write(text)

        # Save binary results.
        res = dict(combis=combis, I10_means=I10_means, I10_stds=I10_stds, I90_means=I90_means, I90_stds=I90_stds)
        pickle.dump(res, open('{}.pck'.format(file_path_base), 'wb'))


# Otherwise, import previous results.
else:
    res = pickle.load(open('{}.pck'.format(file_path_base), 'rb'))
    combis = res['combis']
    I10_means = res['I10_means']
    I10_stds = res['I10_stds']
    I90_means = res['I90_means']
    I90_stds = res['I90_stds']

# Plotting section.

# Define global plotting parameters.
pp = dict()
pp['ylabel'] = 'normalized amplitude'
pp['with_xtick'] = False
pp['with_xaxis'] = False
pp['cap_size'] = 5.0
pp['cap_thick'] = 1.0
pp['colors'] = Plotter.get_colors('inferno', 6, lthresh=0.1, hthresh=0.9) * 4

# Create figure.
plotter = Plotter()
fig, axs = plotter.build_figure(2, ncol=2)

# Plot thresholds.
for combi, ax, ylims in zip(combis, axs, [[0.0, 22.0], [0.0, 110.0]]):
    pp['ax_title'] = 'Threshold / Saturation ({})'.format(combi)
    pp['ylim'] = ylims
    plotter.plot_bars_with_errors(ax,
                                  1,  I10_means[combi][0], I10_stds[combi][0],
                                  2,  I10_means[combi][1], I10_stds[combi][1],
                                  3,  I10_means[combi][2], I10_stds[combi][2],
                                  4,  I10_means[combi][3], I10_stds[combi][3],
                                  5,  I10_means[combi][4], I10_stds[combi][4],
                                  6,  I10_means[combi][5], I10_stds[combi][5],
                                  8,  I90_means[combi][0], I90_stds[combi][0],
                                  9,  I90_means[combi][1], I90_stds[combi][1],
                                  10, I90_means[combi][2], I90_stds[combi][2],
                                  11, I90_means[combi][3], I90_stds[combi][3],
                                  12, I90_means[combi][4], I90_stds[combi][4],
                                  13, I90_means[combi][5], I90_stds[combi][5],
                                  **pp)

plotter.tight_layout(pad=4)

# Legends.
fig.legend(axs[0].patches, entities, ncol=6, loc='lower center', bbox_to_anchor=(0.5, 0.0))

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
