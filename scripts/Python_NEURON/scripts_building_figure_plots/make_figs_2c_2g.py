# -*- coding: utf-8 -*


#############################################################################
# Script :  .../make_figs_2c_2g.py
#
# Created on : 26 September 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note:
#   This script should be executed after the script 'Ia_2.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.utils.Bootstrap import Bootstrap
from smcees.utils.Plotter import Plotter
from smcees.utils import maths


# Choose and load sensorimotor circuit dataset.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C5_C6_C7_C8_T1_Ias')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C5_C6_C7_C8_T1_Ias')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose entity.
# Choose electrodes.
# Choose stimulation amplitudes.
entity = 'Ia'
combis = ['elec10', 'elec11']
amps = -25.0 * np.linspace(1, 80, num=80)
pop_size = 10000
stim_type = 'eext'

# Basename for figure/text/binary files.
folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name_base = 'figs_2c_2g'
file_path_base = os.path.join(folder_path, file_name_base)

# Choose mode:
# 1. compute new recruitment curves and build plots (new results can be saved or not)
# 2. import previous recruitment curves and build plots
mode = 1
save = True

# If mode=1, compute new recruitment curves.
if mode == 1:

    segs = smc.segs
    segs_gidxs = [[gidx for gidx in smc.get_gidxs(entity, seg=seg)] for seg in segs]

    # Load entity records and build dictionnary to cache the retrieved recruitment values.
    records = dict()
    recrs = dict()
    for gidxs in segs_gidxs:
        for gidx in gidxs:
            records[gidx] = smc.load_records(entity, gidx=gidx)
            recrs[gidx] = dict()
            for combi in combis:
                recrs[gidx][combi] = dict()

    # Compute recruitment curves for every combi using a Bootstrapping approach.
    combis_recs = list()
    combis_stds = list()

    for combi in combis:

        segs_recs = list()
        segs_stds = list()

        for s, seg in enumerate(segs):

            seg_recs = np.zeros(amps.size)
            seg_stds = np.zeros(amps.size)

            gidxs = segs_gidxs[s]
            bstrap = Bootstrap(pop_size=pop_size)
            bstrap.set_data_sequence(gidxs)
            bstrap.gen_pop()

            for i, amp in enumerate(amps):

                print('{} | {} | {:f}'.format(combi, seg, amp))

                amp_recs = np.zeros((len(gidxs), pop_size))
                amp_stds = np.zeros((len(gidxs), pop_size))

                for j in range(pop_size):

                    for k, gidx in enumerate(bstrap.pop[:, j]):
                        try:
                            amp_recs[k, j] = recrs[gidx][combi][amp]
                        except:
                            record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)
                                                       & (records[gidx].stim_type == stim_type)]
                            recrs[gidx][combi][amp] = 1.0 if record['recr'].any() else 0.0
                            amp_recs[k, j] = recrs[gidx][combi][amp]

                amp_recs = np.mean(amp_recs, axis=0)
                seg_recs[i] = np.mean(amp_recs)
                seg_stds[i] = np.std(amp_recs)

            segs_recs.append(seg_recs)
            segs_stds.append(seg_stds)

        combis_recs.append(segs_recs)
        combis_stds.append(segs_stds)

    # Find activation amplitude thresholds.
    ratio = 0.1
    thresholds = []
    for segs_recs in combis_recs:
        for seg_recs, seg_gidxs in zip(segs_recs, segs_gidxs):
            try:
                iIxxh = np.flatnonzero(seg_recs)[0]
                iIxxl = np.flatnonzero(seg_recs)[-1]
                Ixxh = amps[iIxxh]
                Ixxl = amps[iIxxl]
                Axxh = seg_recs[iIxxh]
                Axxl = seg_recs[iIxxl]
                if Ixxh == Ixxl:
                    Ixx = Ixxl
                elif Axxh == Axxl:
                    Ixx = Ixxl
                else:
                    Ixx = Ixxl + (Ixxh - Ixxl) / (Axxh - Axxl) * (ratio - Axxl)
            except:
                Ixx = amps[-1]
            thresholds.append(Ixx)
    Ixx0 = max(thresholds)  # amplitudes are negative until here

    # Normalize amplitudes.
    amps = amps / Ixx0

    # Save results.
    if save:

        # Write data to textfile.
        text = ''
        text += 'Amplitudes\n'
        text += ' '.join(['{:.1f}'.format(amp) for amp in amps])
        text += '\n'
        text += 'Recruitment Curves\n'
        for combi, segs_recs, segs_stds in zip(combis, combis_recs, combis_stds):
            text += '{}\n'.format(combi)
            for musc, seg_recs, seg_stds in zip(segs, segs_recs, segs_stds):
                text += '{}\n'.format(segs)
                text += 'Normalized Mean recruitments\n'
                text += ' '.join(['{:.4f}'.format(rec) for rec in seg_recs])
                text += '\n'
                text += 'Normalized STDS recruitments\n'
                text += ' '.join(['{:.4f}'.format(std) for std in seg_stds])
                text += '\n'
            text += '\n'
        with open('{}.txt'.format(file_path_base), 'w') as fileID:
            fileID.write(text)

        # Save binary results.
        res = dict(segs=segs, combis_recs=combis_recs, combis_stds=combis_stds, combis=combis, amps=amps)
        pickle.dump(res, open('{}.pck'.format(file_path_base), 'wb'))


# Otherwise, import previous results.
else:
    res = pickle.load(open('{}.pck'.format(file_path_base), 'rb'))
    segs = res['segs']
    combis_recs = res['combis_recs']
    combis_stds = res['combis_stds']
    combis = res['combis']
    amps = res['amps']


# Smooth curves.
for i in range(len(combis)):
    for j in range(len(segs)):
        combis_recs[i][j] = maths.running_mean(combis_recs[i][j])
        combis_stds[i][j] = maths.running_mean(combis_stds[i][j])


# Plotting section.

# Define global plotting parameters.
pp = dict()
pp['xlabel'] = 'normalized amplitude (x threshold)'
pp['ylabel'] = 'activated fibers (%)'
pp['labels'] = segs
pp['colors'] = Plotter.get_colors('inferno', len(segs), lthresh=0.1, hthresh=0.9)
pp['styles'] = ['-'] * len(segs)
pp['alpha_fills'] = [0.5] * len(segs)
pp['ylim'] = [0.0, 1.0]
pp['xlim'] = [0.0, max(amps)]

# Create figure with subplots.
plotter = Plotter(**pp)
fig, axs = plotter.build_figure(2, ncol=2)

# Plot muscle recruitment curves.
# Left column.
for i in range(2):
    pp['ax_title'] = 'Segmental recruitment curves: {}'.format(combis[i])
    segs_recs = combis_recs[i]
    segs_stds = combis_stds[i]
    plist = []
    for j in range(len(segs)):
        plist.append(amps)
        plist.append(segs_recs[j])
        plist.append(segs_stds[j])
    plotter.plot_bounded_lines(axs[i], *plist, **pp)

# Legends.
fig.legend(axs[0].lines, segs, ncol=len(segs), loc='lower center', bbox_to_anchor=(0.5, 0))

# Savefig.
if save:
    plotter.savefig('{}.{}'.format(file_path_base, 'svg'), format='svg', dpi=600, bbox_inches='tight')
    plotter.savefig('{}.{}'.format(file_path_base, 'png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
