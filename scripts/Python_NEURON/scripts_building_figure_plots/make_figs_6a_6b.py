# -*- coding: utf-8 -*-


#############################################################################
# Script :  .../make_figs_6a_6b.py
#
# Created on : 26 September 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script should be executed after the script 'EPSP_analysis.py'.
#
#############################################################################


import os
import numpy as np
import pickle

from smcees.utils.Plotter import Plotter


# Folder where to store results.
folder = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_path = os.path.join(folder, 'figs_6a_6b_epsps.pck')

# Import epsps results and build plots.
res = pickle.load(open(file_path, 'rb'))
t = res['t']
v1s, v2s, v3s = res['v1s'], res['v2s'], res['v3s']
v4s, v5s, v6s = res['v4s'], res['v5s'], res['v6s']
v7s, v8s, v9s = res['v7s'], res['v8s'], res['v9s']
e1s, e2s, e3s = res['e1s'], res['e2s'], res['e3s']
e4s, e5s, e6s = res['e4s'], res['e5s'], res['e6s']
e7s, e8s, e9s = res['e7s'], res['e8s'], res['e9s']
pop_size = len(v1s)

# Format simulation results.
v1, v2, v3 = np.zeros((pop_size, v1s[0].size)), np.zeros((pop_size, v2s[0].size)), np.zeros((pop_size, v3s[0].size))
v4, v5, v6 = np.zeros((pop_size, v4s[0].size)), np.zeros((pop_size, v5s[0].size)), np.zeros((pop_size, v6s[0].size))
v7, v8, v9 = np.zeros((pop_size, v7s[0].size)), np.zeros((pop_size, v8s[0].size)), np.zeros((pop_size, v9s[0].size))
for n in range(pop_size):
    v1[n, :], v2[n, :], v3[n, :] = v1s[n], v2s[n], v3s[n]
    v4[n, :], v5[n, :], v6[n, :] = v4s[n], v5s[n], v6s[n]
    v7[n, :], v8[n, :], v9[n, :] = v7s[n], v8s[n], v9s[n]
mv1, mv2, mv3 = np.mean(v1, axis=0), np.mean(v2, axis=0), np.mean(v3, axis=0)
mv4, mv5, mv6 = np.mean(v4, axis=0), np.mean(v5, axis=0), np.mean(v6, axis=0)
mv7, mv8, mv9 = np.mean(v7, axis=0), np.mean(v8, axis=0), np.mean(v9, axis=0)
sv1, sv2, sv3 = np.std(v1, axis=0), np.std(v2, axis=0), np.std(v3, axis=0)
sv4, sv5, sv6 = np.std(v4, axis=0), np.std(v5, axis=0), np.std(v6, axis=0)
sv7, sv8, sv9 = np.std(v7, axis=0), np.std(v8, axis=0), np.std(v9, axis=0)

me1, me2, me3 = np.mean(e1s), np.mean(e2s), np.mean(e3s)
me4, me5, me6 = np.mean(e4s), np.mean(e5s), np.mean(e6s)
me7, me8, me9 = np.mean(e7s), np.mean(e8s), np.mean(e9s)
se1, se2, se3 = np.std(e1s), np.std(e2s), np.std(e3s)
se4, se5, se6 = np.std(e4s), np.std(e5s), np.std(e6s)
se7, se8, se9 = np.std(e7s), np.std(e8s), np.std(e9s)

# Plotting section.
plotter = Plotter()
fig, axs = plotter.build_figure(2)

# Voltage traces.
pp = dict()
pp['xlabel'] = 'time (ms)'
pp['xlim'] = [5.0, 15.0]
pp['ylabel'] = 'membrane potential (mV)'
pp['ylim'] = [-71.0, -60.0]
pp['colors'] = plotter.get_colors('inferno', 3, hthresh=0.8) * 3
pp['styles'] = ['-', '-', '-', '--', '--', '--', ':', ':', ':']
plotter.plot_bounded_lines(axs[0], t, mv1, sv1, t, mv2, sv2, t, mv3, sv3,
                                   t, mv4, sv4, t, mv5, sv5, t, mv6, sv6,
                                   t, mv7, sv7, t, mv8, sv8, t, mv9, sv9, **pp)

# EPSP barplots.
pp = dict()
pp['ylabel'] = 'maximal EPSP (mV)'
pp['with_xtick'] = False
pp['with_xaxis'] = False
pp['ylim'] = [0.0, 6.0]
pp['cap_size'] = 5.0
pp['cap_thick'] = 1.0
pp['colors'] = plotter.get_colors('inferno', 3, hthresh=0.8) * 3
plotter.plot_bars_with_errors(axs[1], 1, me1, se1,  2, me2, se2,  3, me3, se3,
                                      5, me4, se4,  6, me5, se5,  7, me6, se6,
                                      9, me7, se7, 10, me8, se8, 11, me9, se9, **pp)

# Legends
combis = [ 'nsyn=50/gmax=10.0', 'nsyn=100/gmax=5.0', 'nsyn=150/gmax=3.333',
		  'nsyn=100/gmax=10.0', 'nsyn=200/gmax=5.0', 'nsyn=300/gmax=3.333',
		  'nsyn=150/gmax=10.0', 'nsyn=300/gmax=5.0', 'nsyn=450/gmax=3.333']
fig.legend(axs[0].lines, combis, ncol=2, loc='upper left', bbox_to_anchor=(0.1, 1.0))

fig.tight_layout(pad=1.5)

plotter.savefig(os.path.join(folder, 'figs_6a_6b.svg'), format='svg', dpi=600, bbox_inches='tight')
plotter.savefig(os.path.join(folder, 'figs_6a_6b.png'), format='png', dpi=600, bbox_inches='tight')

plotter.show(block=True)

print('The end.')
