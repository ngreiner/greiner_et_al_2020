# -*- coding: utf-8 -*


#############################################################################
# Script :  .../parallel/CST.py
#
# Created on : 21 August 2018
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script needs to be executed before the scripts 'make_figs_2b_2f_1.py'
#	and 'make_figs_2b_2f_2.py'.
#
#############################################################################


from mpi4py import MPI

import os
import numpy as np

from smcees.recruitment_builders.CSTRecruitmentBuilder import CSTRecruitmentBuilder
from smcees.neural_entities.SMC import SMC


comm = MPI.COMM_WORLD
size_comm = comm.Get_size()
rank = comm.Get_rank()

dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

rbparams = dict()
rbparams['neuralobj_params'] = dict(model='ModelD')
rbparams['gidxs'] = [gidx for gidx in smc.get_CST_gidxs() if gidx % size_comm == rank]
rbparams['combis'] = ['elec10', 'elec11']
rbparams['amps'] = - 25.0 * np.linspace(1, 80, num=80)
rbparams['simu_params'] = dict(use_var_dt=1, tstop=10.0)
rbparams['skip_when_recr'] = True
rbparams['runperm'] = 1
rb = CSTRecruitmentBuilder(smc, **rbparams)
rb.run_recr_serial()

comm.barrier()
if rank == 0:
	print('The end.')
