# -*- coding: utf-8 -*-


#############################################################################
# Script :  .../parallel/EPSP_analysis.py
#
# Created on : 26 September 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script needs to be executed before the script 'make_figs_6a_6b.py'.
#
#############################################################################


from mpi4py import MPI

import os
import numpy as np
import pickle

from smcees.neural_entities.cells.Motoneuron import Motoneuron
from smcees.simulations.MNSimulation import MNSimulation
from smcees.stims.MNStatSynFixedNStim import MNStatSynFixedNStim
from smcees.utils.resources import swc_files


comm = MPI.COMM_WORLD
size_comm = comm.Get_size()
rank = comm.Get_rank()

# Parameters.
pop_size = 100

# Folder where to store results.
folder = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_path = os.path.join(folder, 'figs_6a_6b_epsps.pck')


if rank == 0:

    # Motoneuron.
    mnparams = dict()
    mnparams['diam'] = 55.0
    mnparams['swc_file'] = swc_files[0]
    mnparams['with_axon'] = False
    mnparams['extra_mech'] = False
    mn = Motoneuron(**mnparams)

    # Simu.
    simuparams = dict()
    simuparams['dt'] = 0.005
    simuparams['use_var_dt'] = 0
    simuparams['tstop'] = 30.0
    simu = MNSimulation(mn, **simuparams)
    simu.record_t()
    # Record sec.
    sec = mn.soma.tsecs[0].hsec
    simu.record_v(sec)
    # Init simu.
    simu.init()

    # Run 3 series of simulations characterized by a common specific nsyn * gmax product, and differing by
    # their nsyn and gmax, the product of which is kept constant.
    # We do that a number $pop_size of times to perform statistical analysis, so we need to store the results in
    # lists.
    v1s, v2s, v3s = list(), list(), list()
    e1s, e2s, e3s = list(), list(), list()

    for n in range(pop_size):

        print('rank 0:  n={:d}'.format(n))

        # Seeds for random number generators.
        seed1 = 3 * n + 1
        seed2 = 3 * n + 2
        seed3 = 3 * n + 3

        # Stim1: nsyn=50 | gmax=0.01
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 50
        stimparams['gmax'] = 10.0
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        if n == 0:
            t = np.asarray(simu.get_records('t'))
            idx = np.where(t >= 5.0)
        v1 = np.asarray(simu.get_records(sec, 'v'))
        epsp1 = np.max(v1[idx]) - np.min(v1[idx])
        v1s.append(v1)
        e1s.append(epsp1)

        # Stim2: nsyn=100 | gmax=0.005
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 50
        stimparams['gmax'] = 5.0
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 50
        stimparams['gmax'] = 5.0
        stimparams['seed'] = seed2
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        v2 = np.asarray(simu.get_records(sec, 'v'))
        epsp2 = np.max(v2[idx]) - np.min(v2[idx])
        v2s.append(v2)
        e2s.append(epsp2)

        # Stim3: nsyn=150 | gmax=0.0033
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 50
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 50
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed2
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 50
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed3
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        v3 = np.asarray(simu.get_records(sec, 'v'))
        epsp3 = np.max(v3[idx]) - np.min(v3[idx])
        v3s.append(v3)
        e3s.append(epsp3)

elif rank == 1:

    # Motoneuron.
    mnparams = dict()
    mnparams['diam'] = 55.0
    mnparams['swc_file'] = swc_files[0]
    mnparams['with_axon'] = False
    mnparams['extra_mech'] = False
    mn = Motoneuron(**mnparams)

    # Simu.
    simuparams = dict()
    simuparams['dt'] = 0.005
    simuparams['use_var_dt'] = 0
    simuparams['tstop'] = 30.0
    simu = MNSimulation(mn, **simuparams)
    simu.record_t()
    # Record sec.
    sec = mn.soma.tsecs[0].hsec
    simu.record_v(sec)
    # Init simu.
    simu.init()

    # Run 3 series of simulations characterized by a common specific nsyn * gmax product, and differing by
    # their nsyn and gmax, the product of which is kept constant.
    # We do that a number $pop_size of times to perform statistical analysis, so we need to store the results in
    # lists.
    v4s, v5s, v6s = list(), list(), list()
    e4s, e5s, e6s = list(), list(), list()

    for n in range(pop_size):

        print('rank 1:  n={:d}'.format(n))

        # Seeds for random number generators.
        seed1 = 3 * n + 1
        seed2 = 3 * n + 2
        seed3 = 3 * n + 3

        # Stim4: nsyn=100 | gmax=0.01
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 100
        stimparams['gmax'] = 10.0
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        if n == 0:
            t = np.asarray(simu.get_records('t'))
            idx = np.where(t >= 5.0)
        v4 = np.asarray(simu.get_records(sec, 'v'))
        epsp4 = np.max(v4[idx]) - np.min(v4[idx])
        v4s.append(v4)
        e4s.append(epsp4)

        # Stim5: nsyn=200 | gmax=0.005
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 100
        stimparams['gmax'] = 5.0
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 100
        stimparams['gmax'] = 5.0
        stimparams['seed'] = seed2
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        v5 = np.asarray(simu.get_records(sec, 'v'))
        epsp5 = np.max(v5[idx]) - np.min(v5[idx])
        v5s.append(v5)
        e5s.append(epsp5)

        # Stim6: nsyn=300 | gmax=0.0033
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 100
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 100
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed2
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 100
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed3
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        v6 = np.asarray(simu.get_records(sec, 'v'))
        epsp6 = np.max(v6[idx]) - np.min(v6[idx])
        v6s.append(v6)
        e6s.append(epsp6)

    # Send v4s, v5s, v6s, e4s, e5s and e6s to rank 0.
    comm.send(v4s, dest=0)
    comm.send(v5s, dest=0)
    comm.send(v6s, dest=0)
    comm.send(e4s, dest=0)
    comm.send(e5s, dest=0)
    comm.send(e6s, dest=0)

elif rank == 2:

    # Motoneuron.
    mnparams = dict()
    mnparams['diam'] = 55.0
    mnparams['swc_file'] = swc_files[0]
    mnparams['with_axon'] = False
    mnparams['extra_mech'] = False
    mn = Motoneuron(**mnparams)

    # Simu.
    simuparams = dict()
    simuparams['dt'] = 0.005
    simuparams['use_var_dt'] = 0
    simuparams['tstop'] = 30.0
    simu = MNSimulation(mn, **simuparams)
    simu.record_t()
    # Record sec.
    sec = mn.soma.tsecs[0].hsec
    simu.record_v(sec)
    # Init simu.
    simu.init()

    # Run 3 series of simulations characterized by a common specific nsyn * gmax product, and differing by
    # their nsyn and gmax, the product of which is kept constant.
    # We do that a number $pop_size of times to perform statistical analysis, so we need to store the results in
    # lists.
    v7s, v8s, v9s = list(), list(), list()
    e7s, e8s, e9s = list(), list(), list()

    for n in range(pop_size):

        print('rank 2:  n={:d}'.format(n))

        # Seeds for random number generators.
        seed1 = 3 * n + 1
        seed2 = 3 * n + 2
        seed3 = 3 * n + 3

        # Stim7: nsyn=150 | gmax=0.01
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 150
        stimparams['gmax'] = 10.0
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        if n == 0:
            t = np.asarray(simu.get_records('t'))
            idx = np.where(t >= 5.0)
        v7 = np.asarray(simu.get_records(sec, 'v'))
        epsp7 = np.max(v7[idx]) - np.min(v7[idx])
        v7s.append(v7)
        e7s.append(epsp7)

        # Stim8: nsyn=300 | gmax=0.005
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 150
        stimparams['gmax'] = 5.0
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 150
        stimparams['gmax'] = 5.0
        stimparams['seed'] = seed2
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        v8 = np.asarray(simu.get_records(sec, 'v'))
        epsp8 = np.max(v8[idx]) - np.min(v8[idx])
        v8s.append(v8)
        e8s.append(epsp8)

        # Stim9: nsyn=450 | gmax=0.0033
        stims = list()
        stimparams = dict()
        stimparams['nsyn'] = 150
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed1
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 150
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed2
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        stimparams = dict()
        stimparams['nsyn'] = 150
        stimparams['gmax'] = 3.333
        stimparams['seed'] = seed3
        stim = MNStatSynFixedNStim(mn, **stimparams)
        stims += [stim]
        # Run simu.
        for stim in stims:
            stim.activate()
        simu.reset()
        simu.run()
        # Extract results.
        v9 = np.asarray(simu.get_records(sec, 'v'))
        epsp9 = np.max(v9[idx]) - np.min(v9[idx])
        v9s.append(v9)
        e9s.append(epsp9)

    # Send v4s, v5s, v6s, e4s, e5s and e6s to rank 0.
    comm.send(v7s, dest=0)
    comm.send(v8s, dest=0)
    comm.send(v9s, dest=0)
    comm.send(e7s, dest=0)
    comm.send(e8s, dest=0)
    comm.send(e9s, dest=0)

# Receive v4s, v5s, v6s, v7s, v8s, v9s, e4s, e5s, e6s, e7s, e8s and e9s from ranks 1 and 2.
# Save results.
if rank == 0:
    v4s = comm.recv(source=1)
    v5s = comm.recv(source=1)
    v6s = comm.recv(source=1)
    e4s = comm.recv(source=1)
    e5s = comm.recv(source=1)
    e6s = comm.recv(source=1)
    v7s = comm.recv(source=2)
    v8s = comm.recv(source=2)
    v9s = comm.recv(source=2)
    e7s = comm.recv(source=2)
    e8s = comm.recv(source=2)
    e9s = comm.recv(source=2)
    res = dict(t=t, v1s=v1s, v2s=v2s, v3s=v3s, v4s=v4s, v5s=v5s, v6s=v6s, v7s=v7s, v8s=v8s, v9s=v9s,
                    e1s=e1s, e2s=e2s, e3s=e3s, e4s=e4s, e5s=e5s, e6s=e6s, e7s=e7s, e8s=e8s, e9s=e9s)
    pickle.dump(res, open(file_path, 'wb'))

