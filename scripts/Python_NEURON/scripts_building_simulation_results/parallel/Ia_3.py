# -*- coding: utf-8 -*


#############################################################################
# Script :  .../serial/Ia_3.py
#
# Created on : 21 September 2018
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script needs to be executed before the scripts 'make_fig_6d.py',
#	'make_fig_6f.py', 'MN_StatSynFromIa_1.py' and 'MN_StatSynFromIa_2.py'.
#
#############################################################################


import os
import numpy as np

from smcees.recruitment_builders.IaRecruitmentBuilder import IaRecruitmentBuilder
from smcees.neural_entities.SMC import SMC


dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_Ias_MNs_8muscs_5segs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_Ias_MNs_8muscs_5segs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

rbparams = dict()
rbparams['neuralobj_params'] = dict(model='ModelD')
rbparams['gidxs'] = smc.get_Ia_gidxs()
rbparams['combis'] = ['elec1', 'elec4', 'elec10']
rbparams['amps'] = - 25.0 * np.linspace(1, 80, num=80)
rbparams['simu_params'] = dict(use_var_dt=1, tstop=10.0)
rbparams['skip_when_recr'] = True
rbparams['runperm'] = 1
rb = IaRecruitmentBuilder(smc, **rbparams)
rb.run_recr_serial()

print('The end.')
