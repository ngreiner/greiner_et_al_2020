# -*- coding: utf-8 -*


#############################################################################
# Script :  .../parallel/Ia_recruitment_localization.py
#
# Created on : 30 January 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   This script needs to be executed before the script 'make_fig_2d.py'.
#
#############################################################################


from mpi4py import MPI

import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.neural_entities.fibers.IaFiber import IaFiber
from smcees.stims.IaFiberEextStim import IaFiberEextStim
from smcees.simulations.IaSimulation import IaSimulation


comm = MPI.COMM_WORLD
size_comm = comm.Get_size()
rank = comm.Get_rank()

# Script parameters.
dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C5_C6_C7_C8_T1_Ias')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C5_C6_C7_C8_T1_Ias')
smc = SMC(dir_data=dir_data, dir_res=dir_res)
combi = 'elec10'
amps = -25.0 * np.linspace(1, 60, num=60)

# fibers of interest.
proxigidxs = smc.get_Ia_gidxs(seg='C6')
nproxi = len(proxigidxs)
dist1gidxs = smc.get_Ia_gidxs(seg='C5')
ndist1 = len(dist1gidxs)
dist2gidxs = smc.get_Ia_gidxs(seg='C7')
ndist2 = len(dist2gidxs)

# Folder where to store results.
folder = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
file_name = 'fig_2d_fibers_records.pck'
file_path = os.path.join(folder, file_name)


gidxs = [gidx for gidx in proxigidxs + dist1gidxs + dist2gidxs if gidx % size_comm == rank]
fibers_records = dict()

# Initialize fiber.
fiberparams = dict()
fiberparams['smc'] = smc
fiberparams['gidx'] = gidxs[0]
fiber = IaFiber(**fiberparams)

# Initialize stim.
stimparams = dict()
stimparams['amp'] = - 25.0
stimparams['delay'] = 5.0
stimparams['dur'] = 0.2
stimparams['combi'] = combi
stim = IaFiberEextStim(fiber, **stimparams)

# Initialize simu.
simuparams = dict()
simuparams['tstop'] = 10.0
simuparams['use_var_dt'] = 1
simu = IaSimulation(fiber, **simuparams)
simu.record_t()

# Loop over gidxs.
for gidx in gidxs:
    print('gidx # {:d} ...'.format(gidx))
    fiber.reset(gidx=gidx)
    fibers_records[gidx] = dict()
    for i, amp in enumerate(amps):
        fibers_records[gidx][amp] = dict()
        stim.reset(combi=combi, amp=amp)
        for node in fiber.get_nodes():
            simu.record_spike_times(node)
        stim.activate()
        simu.reset()
        simu.run()
        recr = simu.get_recr()
        if recr['recr'].any():
            label, idx = simu.find_activation_spot()
            fibers_records[gidx][amp]['recr'] = 1.0
            fibers_records[gidx][amp]['label'] = label
            fibers_records[gidx][amp]['idx'] = idx
        else:
            fibers_records[gidx][amp]['recr'] = 0.0

# Send fibers_records to rank0.
if rank != 0:
    comm.send(fibers_records, dest=0)

# Receive fibers_records in rank0, form global fibers_records, and save it.
if rank == 0:
    for k in range(1, size_comm):
        fibers_records.update(comm.recv(source=k))
    pickle.dump(fibers_records, open(file_path, 'wb'))
    print('The end.')
