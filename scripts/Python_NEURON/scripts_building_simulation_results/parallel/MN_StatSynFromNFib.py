# -*- coding: utf-8 -*-


###########################################################################
# Script :  .../parallel/MN_StatSynFromNFib.
#
# Created on : 23 July 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script needs to be executed before the script 'make_fig_6c.py'.
#
###########################################################################


from mpi4py import MPI

import os
import numpy as np

from smcees.neural_entities.SMC import SMC
from smcees.recruitment_builders.MNStatSynFromNFibRecruitmentBuilder import MNStatSynFromNFibRecruitmentBuilder


comm = MPI.COMM_WORLD
size_comm = comm.Get_size()
rank = comm.Get_rank()

dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_MNs_nogeom')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_MNs_nogeom')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

params = dict()
params['neuralobj_params'] = dict(extra_mech=False, with_axon=False)
params['gidxs'] = [gidx for gidx in smc.get_MN_gidxs() if gidx % size_comm == rank]
params['combis'] = [dict(p_connec=0.3, gmax=10.00),
                    dict(p_connec=0.6, gmax=05.00),
                    dict(p_connec=0.9, gmax=03.30),
                    dict(p_connec=0.3, gmax=15.00),
                    dict(p_connec=0.6, gmax=07.50),
                    dict(p_connec=0.9, gmax=05.00),
                    dict(p_connec=0.3, gmax=22.50),
                    dict(p_connec=0.6, gmax=12.25),
                    dict(p_connec=0.9, gmax=07.50)]
params['amps'] = 5 * np.linspace(1, 40, num=40, dtype=int)
params['simu_params'] = dict(tstop=15.0, use_var_dt=1)
params['skip_when_recr'] = True
params['runperm'] = 1
rb = MNStatSynFromNFibRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

comm.barrier()
if rank == 0:
    print('The end.')
