# -*- coding: utf-8 -*


#############################################################################
# Script :  .../parallel/find_gmax_nsyn_muscles.py
#
# Created on : 29 September 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#   The present module allows to retrieve the synaptic conductances (gmax) reported in Table 2.
#
#   For each muscle, gmax was such that it just saturated the MN recruitment at the stimulation amplitude
#   which just recruited all the Ia-fibers of the concerned muscle.
#   The electrode chosen was that facing the root where the concerned muscle possesed the highest number of Ia-fibers.
#   For instance for the DEL, most of its Ia-fibers are in C6, thus we looked for the stimulation amplitude
#   (with elec10, facing root C6) that just saturated the recruitment of all the Ia-fibers of DEL.
#   gmax was thus determined to be the value that just saturated the MN recruitment when all the synapses provided by
#   the Ia-fibers of DEL were activated.
#
#   This module should be executed after the script 'Ia_3.py'.
#
#############################################################################


from mpi4py import MPI

import os
import numpy as np
import pickle

from smcees.neural_entities.SMC import SMC
from smcees.neural_entities.cells.Motoneuron import Motoneuron
from smcees.simulations.MNSimulation import MNSimulation
from smcees.stims.MNStatSynFromIaStim import MNStatSynFromIaStim


def find_amp_Ia_saturation(smc, musc, combi):
    """Find first amplitude of full Ia recruitment of the input musc with the input combi."""
    gidxs = smc.get_gidxs('Ia', musc=musc)
    amps = list()
    for gidx in gidxs:
        records = smc.load_records('Ia', gidx=gidx)
        records = records.loc[(records.combi == combi) & (records.stim_type == 'eext') & (records.recr == 1.0)]
        vals = records['amp'].values
        amps.append(np.min(np.abs(vals)))
    full_rec_amp = max(amps)
    return full_rec_amp


def find_gmax_MN_saturation(smc, musc, combi, amp):
    """Find the gmax value saturating the recruitment of the MNs of the input musc, for the input combi and amp.

    Performs a binary search involving running simulations.
    """

    # Motoneuron model params.
    mnparams = dict()
    mnparams['smc'] = smc
    mnparams['with_axon'] = True
    mnparams['extra_mech'] = False

    # Stim params.
    stimparams = dict()
    stimparams['combi'] = combi
    stimparams['amp'] = - amp
    stimparams['mode'] = 'fcd'

    # Simu params.
    simuparams = dict()
    simuparams['tstop'] = 15.0
    simuparams['use_var_dt'] = 1

    # Choose initial gmax, initial increment, and increment threshold of the search.
    stimparams['gmax'] = 3.0
    incr = 2.0
    thresh = 0.05
    old_gmax = None

    # We run simulations with every MN of the input musc.
    gidxs = smc.get_gidxs('MN', musc=musc)

    # When gmax is too low to recruit a MN, we increment it by incr.
    # When all the MNs are recruited by a certain gmax, we divide incr by 4.
    # If incr is lower than the specified threshold, the search ends.
    # Otherwise, we start again the search from gmax - (3/4) * incr, and only for those MNs which might be not
    # recruited for that gmax.
    while True:
        print('Start loop. (musc = {} | incr = {:f})'.format(musc, incr))
        while gidxs:
            gidx = gidxs.pop(0)
            mnparams['gidx'] = gidx
            mn = Motoneuron(**mnparams)
            stim = MNStatSynFromIaStim(mn, **stimparams)
            simu = MNSimulation(mn, **simuparams)
            simu.record_t()
            stim.activate()
            simu.init()
            simu.run()
            recr = simu.get_recr().recr.values[0]
            while recr != 1.0:
                ht_gidx = gidx
                stimparams['gmax'] += incr
                print('Incrementing gmax. (musc = {} | gmax ={:f})'.format(musc, stimparams['gmax']))
                stim.reset(gmax=stimparams['gmax'])
                stim.activate()
                simu.reset()
                simu.run()
                recr = simu.get_recr().recr.values[0]
            if stimparams['gmax'] == old_gmax:
                break
        # Update increment.
        incr = incr / 4.0
        if incr <= thresh:
            break
        else:
            old_gmax = stimparams['gmax']
            gidxs = list(range(ht_gidx, gidx + 1))
            stimparams['gmax'] -= 3.0 * incr

    return stimparams['gmax']


def find_mean_nsyn(smc, musc, combi, amp):
    """Compute the mean number of synapses per MN for the input musc with the input combi and amp."""

    # Motoneuron model params.
    mnparams = dict()
    mnparams['smc'] = smc
    mnparams['with_axon'] = False
    mnparams['extra_mech'] = False

    # Stim params.
    stimparams = dict()
    stimparams['combi'] = combi
    stimparams['amp'] = amp

    gidxs = smc.get_gidxs('MN', musc=musc)
    nsyns = np.zeros(len(gidxs))
    for i, gidx in enumerate(gidxs):
        mnparams['gidx'] = gidx
        mn = Motoneuron(**mnparams)
        stim = MNStatSynFromIaStim(mn, **stimparams)
        nsyns[i] = len(stim.syns)

    return nsyns.mean()


if __name__ == '__main__':

    comm = MPI.COMM_WORLD
    size_comm = comm.Get_size()
    rank = comm.Get_rank()

    # Folder where to store results.
    folder_path = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'simulation_results')
    file_name_base = 'gmax_nsyn_muscles'

    dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets')
    dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_Ias_MNs_8muscs_5segs')
    dir_res = os.path.join(dir_smcd, 'results', 'smcd_Ias_MNs_8muscs_5segs')
    smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=0)

    muscs = ['DEL', 'BIC', 'TRI', 'EDC', 'ECR', 'FDS', 'FCR', 'ABP']
    combis = ['elec10', 'elec10', 'elec4', 'elec4', 'elec10', 'elec1', 'elec7', 'elec1']

    gmax_nsyn_muscles = {'gmax': dict(), 'nsyn': dict()}
    for musc, combi in [(muscs[i], combis[i]) for i in range(len(muscs)) if i % size_comm == rank]:
        amp = find_amp_Ia_saturation(smc, musc, combi)
        gmax = find_gmax_MN_saturation(smc, musc, combi, amp)
        gmax_nsyn_muscles['gmax'][musc] = gmax
        nsyn = find_mean_nsyn(smc, musc, combi, amp)
        gmax_nsyn_muscles['nsyn'][musc] = nsyn


    if rank != 0:
        comm.send(gmax_nsyn_muscles['gmax'], dest=0)
        comm.send(gmax_nsyn_muscles['nsyn'], dest=0)

    # Receive fibers_records in rank0, form global fibers_records, and save it.
    if rank == 0:
        for k in range(1, size_comm):
            gmax_nsyn_muscles['gmax'].update(comm.recv(source=k))
            gmax_nsyn_muscles['nsyn'].update(comm.recv(source=k))
        pickle.dump(gmax_nsyn_muscles, open(os.path.join(folder_path, '{}.pck'.format(file_name_base)), 'wb'))
        with open(os.path.join(folder_path, '{}.txt'.format(file_name_base)), 'w') as file:
            file.write('gmax\n')
            file.write(str(gmax_nsyn_muscles['gmax']))
            file.write('\nnsyn\n')
            file.write(str(gmax_nsyn_muscles['nsyn']))
        print('The end.')
