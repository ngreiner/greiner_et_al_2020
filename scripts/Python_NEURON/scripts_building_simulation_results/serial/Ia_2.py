# -*- coding: utf-8 -*


#############################################################################
# Script :  .../serial/Ia_2.py
#
# Created on : 26 September 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script needs to be executed before the scripts 'make_figs_2c_2g.py'
#	and 'make_fig_2h.py'.
#
#############################################################################


import os
import numpy as np

from smcees.recruitment_builders.IaRecruitmentBuilder import IaRecruitmentBuilder
from smcees.neural_entities.SMC import SMC


dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets') 
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C5_C6_C7_C8_T1_Ias')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C5_C6_C7_C8_T1_Ias')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

rbparams = dict()
rbparams['neuralobj_params'] = dict(model='ModelD')
rbparams['gidxs'] = smc.get_Ia_gidxs()
rbparams['combis'] = ['elec{:d}'.format(i) for i in [1, 2, 4, 5, 7, 8, 10, 11, 13, 14]]
rbparams['amps'] = - 25.0 * np.linspace(1, 80, num=80)
rbparams['simu_params'] = dict(use_var_dt=1, tstop=10.0)
rbparams['skip_when_recr'] = True
rbparams['runperm'] = 1
rb = IaRecruitmentBuilder(smc, **rbparams)
rb.run_recr_serial()

print('The end.')
