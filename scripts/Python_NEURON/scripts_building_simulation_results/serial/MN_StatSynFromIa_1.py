# -*- coding: utf-8 -*-


#############################################################################
# Script :  .../serial/MN_StatSynFromIa_1.py
#
# Created on : 22 April 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script should be executed after the script 'Ia_3.py'.
#
#	The gmax value of 7.625 was the value found for the EDC muscle when running
#	the script 'find_gmax_nsyn_muscles.py' on the author's system.
#	You may find a different value due to the different random number sequences
#	that will be generated on your system compared to those originally generated
#	on the author's system.
#
#	This script needs to be executed before running the scripts 'make_fig_6e.py'.
#	If you modify the 7.625 value in the present script according to the output
#	of the 'find_gmax_nsyn_muscles.py' on your system, you should also modify
#	it in the script 'make_fig_6e.py'.
#
#############################################################################


import os
import numpy as np

from smcees.neural_entities.SMC import SMC
from smcees.recruitment_builders.MNStatSynFromIaRecruitmentBuilder import MNStatSynFromIaRecruitmentBuilder


dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets') 
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_Ias_MNs_8muscs_5segs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_Ias_MNs_8muscs_5segs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

params = dict()
params['neuralobj_params'] = dict(extra_mech=False, with_axon=True)
params['combis'] = ['elec1', 'elec4', 'elec10']
params['amps'] = -25.0 * np.linspace(1, 80, num=80)
params['simu_params'] = dict(tstop=15.0, use_var_dt=1)
params['skip_when_recr'] = True
params['runperm'] = 1
params['stim_params'] = dict(connec_type='fcd', gmax=7.625)
params['stim_type'] = 'stat_syn_from_Ia_gmax=7.625'

params['gidxs'] = smc.get_MN_gidxs(musc='DEL')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='BIC')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='TRI')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='ECR')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='EDC')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='FDS')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='FCR')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()

params['gidxs'] = smc.get_MN_gidxs(musc='ABP')
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)
rb.run_recr_serial()


print('The End.')
