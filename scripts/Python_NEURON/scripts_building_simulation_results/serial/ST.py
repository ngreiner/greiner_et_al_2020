# -*- coding: utf-8 -*


#############################################################################
# Script :  .../serial/ST.py
#
# Created on : 24 April 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
# Note :
#	This script needs to be executed before the scripts 'make_figs_2b_2f_1.py'
#	and 'make_figs_2b_2f_2.py'.
#
#############################################################################


import os
import numpy as np

from smcees.recruitment_builders.STRecruitmentBuilder import STRecruitmentBuilder
from smcees.neural_entities.SMC import SMC


dir_smcd = os.path.join('..', '..', '..', 'simulation_data', 'Python_NEURON', 'sensorimotor_circuit_datasets') 
dir_data = os.path.join(dir_smcd, 'datasets', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
dir_res = os.path.join(dir_smcd, 'results', 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

rbparams = dict()
rbparams['neuralobj_params'] = dict(model='ModelD')
rbparams['gidxs'] = smc.get_ST_gidxs()
rbparams['combis'] = ['elec10', 'elec11']
rbparams['amps'] = - 25.0 * np.linspace(1, 80, num=80)
rbparams['simu_params'] = dict(use_var_dt=1, tstop=10.0)
rbparams['skip_when_recr'] = True
rbparams['runperm'] = 1
rb = STRecruitmentBuilder(smc, **rbparams)
rb.run_recr_serial()

print('The end.')
