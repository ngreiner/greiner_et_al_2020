# File :  README.txt
#
# Created on : 17 October 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch

The folder 'tracings' contains 2-dimensional digital tracings of the
contours of the grey matter and white matter of the spinal cord.

These were obtained from scanned pictures of cross-sections of the monkey
spinal cord at segmental levels C2 to T6.

The pictures were taken from :
Atlas of the Spinal Cord. Sengul, Watson, Tanaka, Paxinos. 2013. Elsevier.
